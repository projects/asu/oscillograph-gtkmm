%def_without doxygen

Name: liboscillograph-gtkmm
Version: 0.5
Release: eter8

Packager: Pavel Vainerman <pv@etersoft.ru>

Summary: Oscillograph components for gtkmm

Copyright: GPL
Group: System/Libraries
URL: http://setri.ru

Source: %name-%version.tar

#ExclusiveArch: %ix86

# Automatically added by buildreq on Fri May 11 2012
# optimized out: fontconfig fontconfig-devel gcc-c++ glib2-devel libatk-devel libatkmm-devel libcairo-devel libcairomm-devel libcommoncpp2-devel libfreetype-devel libgdk-pixbuf libgdk-pixbuf-devel libgio-devel libglibmm-devel libgtk+2-devel libgtkmm2-devel libomniORB-devel libpango-devel libpangomm-devel libsigc++2-devel libstdc++-devel libxml2-devel pkg-config
BuildRequires: gcc-c++ libplotmm-devel

%description
Oscillograph component for gtkmm projects.
This package provides libraries to use oscillograph component.

%description -l ru_RU.UTF-8
Компонент "осциллограф" для проектов, использующих gtkmm.
Пакет предоставляет библиотеки, используемые с компонентом осциллограф.

%package devel
Group: Development/C
Summary: Libraries needed to develop for Oscillograph component
Summary(ru_RU.UTF-8): Библиотеки, требуемые для разработки с компонентом осциллограф
Requires: %name = %version-%release

%description devel
Libraries needed to develop for planner.
%description devel -l ru_RU.UTF-8
Библиотеки, требуемые для разработки с компонентом осциллограф

%package devel-static
Summary: Libraries for Oscillograph component
Group: System/Libraries
Requires: %name = %version-%release

%description devel-static
This package provides static libraries to use oscillograph component.
%description devel-static -l ru_RU.UTF-8
Пакет предоставляет библиотеки, для статической линковки

%prep
%setup

%build
%autoreconf
%configure %{subst_with doxygen}

%make_build

%install
%makeinstall_std

%files
%_libdir/*.so*

%if_with doxygen
%files doc
%_docdir/%name-%version/
%endif

%files devel
%dir %_includedir/%name
%_includedir/%name/*.h
%_libdir/pkgconfig/*.pc

%files devel-static
%_libdir/*.a


%changelog
* Fri Oct 24 2014 Pavel Vainerman <pv@altlinux.ru> 0.5-eter8
- added Gtk::Viewport for chanel scrolled window 
- rename "ConnectedValuesSialog" --> ConnectedValueDialog
- refactoring: move Frame(lbl_y) to Channel from Oscillograph

* Thu Sep 06 2012 Ilya Polshikov <ilyap@etersoft.ru> 0.5-eter7.6
- New build after merge.

* Fri Aug 24 2012 Aleksey Vinogradov <uzum@server.setri.ru> 0.5-eter7.2
- new build

* Fri Aug 24 2012 Aleksey Vinogradov <uzum@server.setri.ru> 0.5-eter7.1
- new build

* Tue Jun 05 2012 Pavel Vainerman <pv@altlinux.ru> 0.5-eter7
- remove x_step menu

* Mon Jun 04 2012 Pavel Vainerman <pv@altlinux.ru> 0.5-eter6
- sprintf --> snprintf

* Tue May 15 2012 Pavel Vainerman <pv@server> 0.5-eter5
- fixed bug in Oscillograph::tick()

* Fri May 11 2012 Ilya Polshikov <ilyap@etersoft.ru> 0.5-eter4
- new build 0.5-eter4

* Tue Mar 13 2012 Ilya Polshikov <ilyap@etersoft.ru> 0.5-eter3
- new build 0.5-eter3

* Wed Nov 02 2011 Ilya Polshikov <ilyap@etersoft.ru> 0.5-eter2
- new build 0.5-eter2

* Mon Mar 28 2011 Ivan Donchevskiy <yv@etersoft.ru> 0.5-eter1
- new build 0.5-eter1

* Tue Nov 02 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4-eter12
- new build 0.4-eter12

* Sat Oct 30 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4-eter11
- new build 0.4-eter11

* Sat Oct 30 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4-eter10
- new build 0.4-eter10

* Tue Oct 05 2010 Ilya Shpigor <elly@altlinux.org> 0.4-eter9
- new build 0.4-eter9

* Wed Sep 01 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4-eter8
- new build 0.4-eter8

* Tue Aug 10 2010 Ivan Donchevskiy <yv@etersoft.ru> 0.4-eter7
- new build 0.4-eter7

* Thu Jul 22 2010 Ilya Shpigor <elly@altlinux.org> 0.4-eter5
- new build 0.4-eter5

* Tue Jun 01 2010 Vitaly Lipatov <lav@altlinux.ru> 0.4-eter1
- cleanup spec
- repo converted to utf8
- some fixes

* Wed Nov 25 2009 alex@etg10 0.3-setri74
- new build

* Wed Nov 25 2009 alex@etg10 0.3-setri73
- new build

* Wed Nov 25 2009 alex@etg10 0.3-setri72
- new build

* Wed Nov 25 2009 alex@etg10 0.3-setri71
- new build

* Tue Nov 24 2009 alex@etg10 0.3-setri70
- new build

* Tue Nov 24 2009 alex@etg10 0.3-setri69
- new build

* Tue Nov 24 2009 alex@etg10 0.3-setri68
- new build

* Tue Nov 24 2009 alex@etg10 0.3-setri68
- new build

* Wed Nov 24 2009 alex@etg10 0.3-setri67
- new build

* Tue Nov 24 2009 alex@etg10 0.3-setri66
- new build

* Fri Oct 23 2009 alex@etg10 0.3-setri65
- new build

* Thu Oct 22 2009 alex@etg10 0.3-setri64
- new build

* Mon Sep 21 2009 Alex Surov <alex@aeu> 0.3-setri58
- new build

* Mon Sep 21 2009 Alex Surov <alex@aeu> 0.3-setri57
- new build

* Mon Sep 21 2009 Alex Surov <alex@aeu> 0.3-setri56
- new build

* Fri Aug 21 2009 Alex Surov <alex@aeu> 0.3-setri55
- new build

* Fri Aug 21 2009 Alex Surov <alex@aeu> 0.3-setri54
- new build

* Fri Aug 21 2009 Alex Surov <alex@aeu> 0.3-setri53
- new build

* Fri Aug 21 2009 Alex Surov <alex@aeu> 0.3-setri52
- new build

* Tue Aug 18 2009 Alex Surov <alex@aeu> 0.3-setri51
- new build

* Tue Aug 18 2009 Alex Surov <alex@aeu> 0.3-setri50
- new build

* Tue Aug 18 2009 Alex Surov <alex@aeu> 0.3-setri49
- new build

* Mon Aug 17 2009 Alex Surov <alex@aeu> 0.3-setri48
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri47
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri46
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri45
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri44
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri43
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri42
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri41
- new build

* Wed Jun 24 2009 Alex Surov <alex@aeu> 0.3-setri40
- new build

* Mon Jun 22 2009 Alex Surov <alex@aeu> 0.3-setri39
- new build

* Wed May 20 2009 Pavel Vainerman <pv@altlinux.ru> 0.3-setri11
- new build

