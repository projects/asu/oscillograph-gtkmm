#! /bin/sh

# If needed, run autoreconf -fiv manually and commit all files

TryExport()
{
	EXPORTPATH=$(readlink -f $1)
	test -r $1/$2.include && . $1/$2.include
}

# if local arg, build with libraries from git tree
if [ "$1" = "local" ] ; then
	shift
	# Если нужно отключить сборку с локальной библиотекой,
        # лучше переименовать каталог.
	#TryExport ../uniwidgets uniwidgets
fi

# set flags as in rpm build
export CFLAGS='-pipe -O2 -march=i586 -mtune=i686'
export CXXFLAGS="$CFLAGS"

# We run just autoreconf, updates all needed
autoreconf -fiv

./configure --enable-maintainer-mode --prefix=/usr

