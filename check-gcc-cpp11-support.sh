#!/bin/sh

read -r M N V <<< $(g++ -dumpversion | tr '.' ' ')
RET=1

if [ $M -gt "4" ]; then
  echo "detect gcc $M.$N.$V --> std c++11 supported.."
  RET=0
elif [ $M -eq "4" ] && [ $N -ge "8" ]; then
  echo "detect gcc $M.$N.$V --> std c++11 supported.."
  RET=0 
else
  echo "detect gcc $M.$N.$V --> std c++11 not supported.."
  RET=1
fi

exit $RET
