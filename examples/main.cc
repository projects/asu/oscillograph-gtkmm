#include <math.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <iostream>
#include "Oscillograph.h"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
static const int update_data_msec = 100;
unsigned int history_max=0;
long maxpointnum=50;
//--------------------------------------------------------------------------
class Test
{
	public:

		Test():
			draw(true)
		{
			const char* clr[4] = { "blue", "green", "magenta","black" };
			const char* nm[4] = { "cos","2*cos","3*cos","Канал №1" };
			for( int c=0; c<4; c++ )
			{
				//Channel* ch = oscil.add_channel( nm[c], Gdk::Color(clr[c]) );
				Glib::RefPtr<Channel> ch = oscil.add_channel( nm[c], Gdk::Color(clr[c]), 40, maxpointnum);
				ch->set_history_max_kbytes(history_max);
				ch->set_file_history_max_point(maxpointnum);
				ch->set_size(40);
			}
			//!NB! Testing Mode  begin {
			//! Do not forget to change calling of oscil.plot
			//oscil.remove_channel(3);
			//oscil.remove_channel(3);

			//oscil.remove_channel(1);
			//oscil.remove_channel(1);

			oscil.set_legend_width(160);
			oscil.show();
			
			oscil.set_backtrace_enable(true);
			oscil.run();

			oscil.set_time_interval(5);

			tmr = Glib::signal_timeout().connect(
				sigc::mem_fun(*this, &Test::on_time),update_data_msec);

			tmr1 = Glib::signal_timeout().connect(
				sigc::mem_fun(*this, &Test::on_change),2000);
		}
	
		~Test()
		{
			tmr.disconnect();
			tmr1.disconnect();
		}

		bool on_time()
		{
			if( draw )
			{
				double t = oscil.get_time_elapsed();
				oscil.plot(1,t,2*cos(t));
				oscil.plot(2,t,3*cos(t));
				oscil.plot(3,t,4*cos(t));
			}
			return true;
		}

		bool on_change()
		{
			draw ^= true;
			
			if( draw )
				oscil.plot(1, oscil.get_time_elapsed(),5 );
			else
			{
				double t = oscil.get_time_elapsed();
				for(int i=1;i<=3;i++)
					oscil.plot(i, t,0 );
			}
			return true;
		}

		Oscillograph oscil;

private:
		sigc::connection tmr;
		sigc::connection tmr1;
		bool draw;
};

int main(int argc, char** argv)
{
	Gtk::Main kit(argc, argv);
	Gtk::Window win;
	win.set_title("Осциллограф");

	Test t;
	win.add(t.oscil);

	Gtk::Main::run(win);
	return 0;
}
//--------------------------------------------------------------------------
