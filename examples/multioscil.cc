#include <math.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <iostream>
#include "MultiOscillograph.h"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
static const int onum = 2;
int channels=3;
double timer_step=100;
unsigned int history_max=25;
long maxpointnum=4000;
//--------------------------------------------------------------------------
class Test
{
	public:

		Test()
		{
			for( int i=1; i<=onum; i++ )
			{
				Oscillograph* o = mo.add_oscillograph();
				if( !o )
				{
					cerr << " add_oscillograph.. null pointer..." << endl;
					continue;
				}

				const char* clr[9] = { "blue", "green", "red"};
				const char* nm[9] = { "cos","cos(t+30)","cos(t+60)"};
				for( int c=0; c<channels; c++ )
				{
					Glib::RefPtr<Channel> channel = o->add_channel( nm[(c+2)%3], Gdk::Color(clr[(c+2)%3]), "" );
					channel->set_history_max_kbytes(history_max);
					channel->set_file_history_max_point(maxpointnum);
				}
				o->set_plot_size( 455, 200 );
				o->set_legend_width(155);

				/*
					Устанавливает макс. расстояние от курсора до точки кривой, на котором формируется подсказка.
				*/
				o->set_y_tolerance(i*35);
				
				o->show();

				o->set_enable_timer(true);
				o->get_plot()->signal_plot_mouse_press().connect(sigc::mem_fun(*this,&Test::on_oscil_mouse_press));
			}

			mo.show_all_children(true);

			tmr = Glib::signal_timeout().connect(
				sigc::mem_fun(*this, &Test::on_time),100);

			mo.set_timer_step(timer_step);
		}
	
		void on_oscil_mouse_press( int x, int y, GdkEventButton* ev )
		{
			if( (ev->type == Gdk::DOUBLE_BUTTON_PRESS) && (ev->button == 1) )
				cout << "********** on_event_clicked() double clicked..." << endl;
		}
	
		~Test()
		{
			tmr.disconnect();
		}

		bool on_time()
		{
			for( int i=1; i<=onum; i++ )
			{		
				Oscillograph* o = mo.get_oscillograph(i);
				if( !o )
				{
					cout << "(" << i << "): null pointer..." << endl;
					continue;
				}

				double t = o->get_time_elapsed();

				for( int c=0; c<channels; c=c+3 )
				{
				    o->plot(c+1,t,2*cos(t));
				    o->plot(c+2,t,2*cos(t+30.0));
				    o->plot(c+3,t,2*cos(t+60.0));
				}
			}

			return true;
		}

		MultiOscillograph mo;
		sigc::connection tmr;
};

int main(int argc, char** argv)
{
	Gtk::Main kit(argc, argv);
	Gtk::Window win;
	Gtk::HBox hbox;
	hbox.show();
	win.set_title("MultiOscillograph");

	Test t;
	hbox.add(t.mo);
	win.add(hbox);
	Gtk::Main::run(win);
	return 0;
}
//--------------------------------------------------------------------------
