//$Id: Channel.cc,v 1.38 2010/02/04 08:08:59 alex Exp $
//--------------------------------------------------------------------------
#include <math.h>
#include "Channel.h"
#include "Dialogs.h"
#include <errno.h>
#include <iomanip>

//--------------------------------------------------------------------------
Channel::Channel(std::string filename, std::string tmpdir, int size, int filesz):
	mwin(0),
	to_CodePage("CP1251"),
	from_CodePage("UTF8"),
	outfileIsOpen(0),
	size(size),
	cur_history_record(0),
	history_size(0),
	enable(true),
	backtrace(false),
	clr(Gdk::Color("black")),
	y_fmt("%0.02f"),
	m_Menu_Popup(),
	last_x(0),
	last_y(0),
	rewrite(true),
	is_writing(false),
	write_enabled(true)
{
	set_history_max_kbytes(	0 );

	//btn = manage( new Gtk::CheckButton("noname") );
	btn = new Gtk::CheckButton("noname");
	btn->set_active(true);
//	btn->signal_button_press_event().connect(sigc::mem_fun(*this,&Channel::on_button_press_event));
	btn_toggled = btn->signal_toggled().connect(sigc::mem_fun(*this,&Channel::on_btn_toggled));
	btn_event = btn->signal_event_after().connect(sigc::mem_fun(*this,&Channel::on_event_after));
	btn->signal_add().connect(sigc::mem_fun(*this,&Channel::on_add));
	btn->show();

	//lbl_y = manage( new Gtk::Label("") );
	lbl_y = new Gtk::Label("");
	lbl_y->show();
	lbl_y->set_text("0.00");
//	lbl_y->set_size_request(80,-1);
	lbl_y->set_padding(1,1);

	frm_y = new Gtk::Frame();
	frm_y->add(*lbl_y);
	frm_y->show();

		Gtk::Menu::MenuList& menulist = m_Menu_Popup.items();

		mi_color = manage( new Gtk::MenuItem(("Выбрать цвет..."),false) );
		mi_color->signal_activate().connect(sigc::mem_fun(*this, &Channel::on_menu_select_color));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_color) );
		mi_color->show();

//		mi_bufsize = manage( new Gtk::MenuItem(("Размер буфера хранения..."),false) );
//		mi_bufsize->signal_activate().connect(sigc::mem_fun(*this, &Channel::on_menu_bufsize));
//		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_bufsize) );
//		mi_bufsize->show();

	curve = Glib::RefPtr<PlotMM::Curve>( new PlotMM::Curve("noname") );
	curve->set_curve_style( PlotMM::CURVE_LINES );

	historyfilename="";
	pointnum = 0;
	filesize = filesz;

	outfile = 0;
	if(filename.empty())
		return;

	char curdir[FILENAME_MAX];
	char* result=getcwd(curdir,FILENAME_MAX);
	if (result==NULL)
		std::cerr<<"Слишком длинный путь. Channel.cc::77"<<std::endl;

	if(tmpdir.empty())
		tmpdir = curdir;
	else if(!createDir(tmpdir))
		tmpdir = curdir;

	folder = tmpdir;
	historyfiledir	= tmpdir + "/";
	historyfilename	= filename;

	historyfilefullname = historyfiledir+"."+historyfilename+".dat";
	stor = new StorInterface();
}
//--------------------------------------------------------------------------
Channel::~Channel()
{
	delete btn;
	btn=0;
	delete lbl_y;
	lbl_y=0;
}
//------------------------------------------------------------------------------
void Channel::set_file_history_max_point( long value )
{
	filesize = value;
	if(!filesize)
		return;
}
//------------------------------------------------------------------------------
bool Channel::createDir( const std::string dir )
{
	struct stat st;
	ostringstream cmd;
	if( stat(dir.c_str(), &st) )
	{
		cout << "ERRNO = "<<errno << endl;
		if( errno == ENOENT )
			return false;
	}

	if( mkdir(dir.c_str(), (S_IRWXO | S_IRWXU | S_IRWXG) ) == -1 )
	{
		if( errno != EEXIST )
		{
			cerr << "mkdir " << dir << " FAILED! (" << strerror(errno) << endl;
			Gtk::MessageDialog mdialog(("(mkdir): Не удалось создать папку " +dir),true,Gtk::MESSAGE_WARNING);
			mdialog.run();
			return false;
		}
	}
	
	return true;
}
//--------------------------------------------------------------------------
void Channel::set_folder(string str)
{
	folder=str;
}
//--------------------------------------------------------------------------
void Channel::set_to_CodePage(string str)
{
	to_CodePage=str;
}
//--------------------------------------------------------------------------
void Channel::set_from_CodePage(string str)
{
	from_CodePage=str;
}
//--------------------------------------------------------------------------
void Channel::draw( double x, double y )
{
	if( x==last_x && y == last_y )
		return;

	// небольшая оптимизация
	if( last_y != y )
	{
		if( lbl_y->is_realized() )
		{
			snprintf(vbuf,sizeof(vbuf),y_fmt.c_str(),y);
			Glib::ustring t(vbuf);
			lbl_y->set_text(t);
//			lbl_y->queue_resize_no_redraw();
//			lbl_y->queue_draw();
		}
	}
	last_x = x;
	last_y = y;

	Time tmpTime = getTime();

	if( data.size() >= size )
	{
		History tmpHistory;
		tmpHistory.time	= historyT.begin()->second;
		tmpHistory.x	= (*(data.begin())).get_x();
		tmpHistory.y	= (*(data.begin())).get_y();
		
		if(filesize && is_writing && write_enabled)
		{
			if( history_size<=1 )
			{
				if( !ifEOF() )
					return;
				if( !stor->write(tmpHistory) )
					cerr << "Ошибка записи" << historyfilefullname << endl;
			}
			else if( cur_history_record==history_size-1 )
			{
				cur_history_record = 0;
				for(std::list<History>::iterator it=history.begin(); it!=history.end(); ++it )
				{
					if( !ifEOF() )
						return;
					if( !stor->write(*it) )
						cerr << "Ошибка записи" << historyfilefullname << endl;
					else
						pointnum++;
				}
			}
			else
				cur_history_record++;
		}
		// удаляем устаревший элемент (первый)
		if(!data.empty())
		data.erase( data.begin() );
		if(!historyT.empty())
		historyT.erase( historyT.begin() );
	}
	data.push_back(PlotMM::DoublePoint(x,y));
	historyT[x] = tmpTime;
}
//--------------------------------------------------------------------------
void Channel::set_enabled( bool st )
{
	enable = st;
	curve->set_enabled(st);
	if( btn->get_active() != st )
		btn->set_active(st);
}
//--------------------------------------------------------------------------
void Channel::replot()
{
	curve->set_data(data);
}
//--------------------------------------------------------------------------
void Channel::clean()
{
	data.clear();
	replot();
}
//--------------------------------------------------------------------------
void Channel::on_event_after( GdkEvent* ev )
{
	if( (ev->type == GDK_BUTTON_PRESS) && (ev->button.button == 3) )
		m_Menu_Popup.popup(ev->button.button, ev->button.time );
}
//--------------------------------------------------------------------------
void Channel::set_color( Gdk::Color _clr )
{
	clr = _clr;
	curve->paint()->set_pen_color(clr);

#warning Спорный метод доступа к label на кнопке...
	Glib::ListHandle<Gtk::Widget*> child = btn->get_children();
	for( Glib::ListHandle<Gtk::Widget*>::iterator cit=child.begin();
			cit != child.end(); ++cit )
	{
//		cout << "name=" << (*cit)->get_name() << endl;
		if( (*cit)->get_name() == "GtkLabel" )
		{
			(*cit)->modify_fg(Gtk::STATE_NORMAL,clr);
			(*cit)->modify_fg(Gtk::STATE_ACTIVE,clr);
			break;
		}
	}
}
//--------------------------------------------------------------------------
void Channel::set_size( unsigned int sz )
{
	if( sz == size )
		return;

	if( data.empty() )
	{
		size = sz;
		return;
	}

	if( size > sz )
	{
		// удаляем лишние
		int sub = (size - sz);
		if( sub > (int)data.size() )
		{
			data.clear();
		}
		else
		{
			for(int i=0; i<sub; i++ )
				data.erase( data.begin() );
		}
	}
	size = sz;
}
//--------------------------------------------------------------------------
void Channel::set_history_max_kbytes( unsigned int kbytes )
{
	set_history_max_size( (kbytes*1024) / sizeof(History) );
}
//--------------------------------------------------------------------------
unsigned int Channel::get_history_max_kbytes()
{	
//	return ( history_size * sizeof(PlotMM::DoublePoint) / 1000 );
	return ( history_size * sizeof(History) / 1024 );
}
//--------------------------------------------------------------------------
void Channel::set_history_max_size( unsigned int sz )
{
	if( sz == history_size )
		return;

	if( history.empty() )
	{
		history_size = sz;
		return;
	}

	if( history_size > sz )
	{
		// удаляем лишние
		int sub = (history_size - sz);
		if( sub > (int)history.size() )
			history.clear();
		else
		{
			for( int i=0; i<sub; i++ )
			{
				history.erase( history.begin() );
//				historyT.erase( historyT.begin() );
			}
		}
	}

	history_size = sz;
}
//--------------------------------------------------------------------------
void Channel::set_title( const Glib::ustring s )
{
	btn->set_label(s);
	curve->set_title(s);
}
//--------------------------------------------------------------------------
void Channel::on_menu_select_color()
{
	Gtk::ColorSelectionDialog d(("Выбор цвета..."));
	if(mwin)
	{
		d.set_parent(*mwin);
//		d.set_size_request(mwin->get_screen()->get_width(), mwin->get_screen()->get_height()); 
	}
//	else
//		d.set_size_request(1280, 768); 
	d.set_modal(true);
	d.set_position(Gtk::WIN_POS_CENTER);
//	d.maximize();
//	d.set_decorated(false);
	d.get_colorsel()->set_current_color(clr);
	int res = d.run();
	switch(res)
	{
		case Gtk::RESPONSE_OK:
			set_color( d.get_colorsel()->get_current_color() );
		break;	
	}
}
//--------------------------------------------------------------------------
void Channel::on_menu_set_k()
{

}
//--------------------------------------------------------------------------
void Channel::on_add( Gtk::Widget* w )
{
//	cout << "on_add: " << w->get_name() << endl;
	time_t tm=time(NULL);
	struct tm *tms = localtime(&tm);
	tm_hour = tms->tm_hour;
	tm_min = tms->tm_min;
	tm_sec = tms->tm_sec;

	if( w->get_name() == "GtkLabel" )
	{
		w->modify_fg(Gtk::STATE_NORMAL,clr);
		w->modify_fg(Gtk::STATE_ACTIVE,clr);
	}
}
//--------------------------------------------------------------------------
void Channel::on_btn_toggled()
{
	set_enabled( btn->get_active() );
}
//--------------------------------------------------------------------------
bool Channel::on_button_press_event(GdkEventButton *ev)
{
	return true;
}
//--------------------------------------------------------------------------
string Channel::dateToString(time_t tm, string brk)
{
    struct tm *tms = localtime(&tm);
	ostringstream date;
	date << std::setw(4) << std::setfill('0') << tms->tm_year+1900 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mon+1 << brk;
	date << std::setw(2) << std::setfill('0') << tms->tm_mday;	
	return date.str();
}
//--------------------------------------------------------------------------
string Channel::timeToString(time_t tm, string brk)
{
    struct tm *tms = localtime(&tm);
	ostringstream time;
	time << std::setw(2) << std::setfill('0') << tms->tm_hour << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_min << brk;
	time << std::setw(2) << std::setfill('0') << tms->tm_sec;	
	return time.str();
}
//--------------------------------------------------------------------------
void Channel::timeToInt(long time,int &hour, int &min, int &sec)
{
	hour = tm_hour + time / 3600;
	while(hour>=24)
		hour = hour - 24;
	min = tm_min + (time % 3600) / 60;
	if(min>=60)
	{
		min = min - 60;
		hour++;
		if(hour>=24)
			hour = hour - 24;
	}
	sec = tm_sec + (time % 3600) % 60;
	if(sec>=60)
	{
		sec = sec - 60;
		min++;
		if(min>=60)
		{
			min = min - 60;
			hour++;
			if(hour>=24)
				hour = hour - 24;
		}
	}
}

//--------------------------------------------------------------------------
void Channel::set_backtrace( bool bt )
{
	backtrace = bt;
}
//--------------------------------------------------------------------------
void Channel::on_menu_bufsize()
{
	InputValueDialog d("Размер буфера хранения истории");
	if(mwin)
	{
		d.get_dialog()->set_parent(*mwin);
//		d.get_dialog()->set_size_request(mwin->get_screen()->get_width(), mwin->get_screen()->get_height()); 
	}
//	else
//		d.get_dialog()->set_size_request(1280, 768); 
	d.get_dialog()->set_modal(true);
	d.get_dialog()->set_position(Gtk::WIN_POS_CENTER);
//	d.get_dialog()->maximize();
//	d.get_dialog()->set_decorated(false);
	d.set_increments(1.0,10.0,0);
	set_history_max_kbytes( lround( d.run( get_history_max_kbytes(), 0, 10000,"Kbytes") ) );
}
//--------------------------------------------------------------------------
string Channel::set_format_y_label( std::string fmt )
{
	string old = y_fmt;
	y_fmt = fmt;
	return old;
}
//--------------------------------------------------------------------------
void Channel::show_y_label( bool state )
{
	if( state )
		frm_y->show();
	else
		frm_y->hide();
}
//--------------------------------------------------------------------------
void Channel::menu_color_set_sensitive( bool state )
{
	mi_color->set_sensitive(state);
}
//--------------------------------------------------------------------------
//void Channel::menu_save_set_sensitive( bool state )
//{
//	mi_save->set_sensitive(state);
//}
//--------------------------------------------------------------------------
void Channel::menu_bufsize_set_sensitive( bool state )
{
	mi_bufsize->set_sensitive(state);
}
//--------------------------------------------------------------------------
Time Channel::getTime()
{
	time_t tm=time(NULL);
	struct tm *tms = localtime(&tm);
	Time tmpTime;
	tmpTime.hour = tms->tm_hour;
	tmpTime.min = tms->tm_min;
	tmpTime.sec = tms->tm_sec;
	return tmpTime;
}
//--------------------------------------------------------------------------
bool Channel::readHistory(int num, History& hist)
{
	return stor->read(num, hist);
}
//--------------------------------------------------------------------------
bool Channel::readNextHistory(History& hist)
{
	#warning метод не реализован!Доступ к следующему элементу истории!
	return false;
}
//--------------------------------------------------------------------------
void Channel::goHistoryHead()
{
	#warning метод не реализован!Доступ к первому элементу истории.Использовался итератор!
}


double Channel::into_the_curve(double x)
{
	if (enable)
	{
		std::list<PlotMM::DoublePoint>::reverse_iterator iter_current, iter_previous;
		iter_current=data.rbegin();
		iter_previous=data.rbegin();
		for(iter_current++; iter_current!= data.rend(); iter_current++, iter_previous++)
		{
			if (x<=iter_previous->get_x() && x>=iter_current->get_x())
			{
				//создаем уравнение получившегося отрезка, подставляем
				//туда аргумент x, и смотрим, насколько реальный y отличается
				//от посчитанного
				double y2=iter_previous->get_y();
				double x2=iter_previous->get_x();
				double y1=iter_current->get_y();
				double x1=iter_current->get_x();
				return (y2-y1)/(x2-x1)*(x-x1) + y1 ;
			}
		}
	}
	return -1;
}
//--------------------------------------------------------------------------
bool Channel::ifEOF()
{
	if( !rewrite && stor->isFull() )
	{
		//Wake up the screen
		cout << "waking up: " << system("xset dpms force on && xset s reset") << endl;

		write_enabled = false;
		string mesg = "Временный файл хранения данных канала " + historyfilename + "закончился. Сохранить накопленные данные?";
		Gtk::MessageDialog msg(mesg,false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_YES_NO);
		msg.set_modal( true );
		msg.set_position( Gtk::WIN_POS_CENTER );
		int result = msg.run();
		switch(result)
		{
			case(Gtk::RESPONSE_YES):
				save_channel_with_save_dialog(historyfilename,historyfilename,historyfiledir);
				break;
			case(Gtk::RESPONSE_NO):
			default:
				break;
		}
		if( !stor->create(historyfilefullname, filesize, sizeof(History)) )
		{
			cerr << "failed to create history file" << endl;
			write_enabled = true;
			return false;
		}
		write_enabled = true;
	}
	return true;
}
//--------------------------------------------------------------------------
bool Channel::startWriting()
{
	if( !stor->create(historyfilefullname, filesize, sizeof(History)) )
	{
		cerr << "failed to create history file" << endl;
		return ( is_writing=false );
	}
	Gtk::Menu::MenuList& mlist = m_Menu_Popup.items();
	mlist[2].set_label(STOP_WRITING_CHANNEL_TEXT);
	mlist[13].hide();
	mlist[14].hide();
	mlist[15].hide();
	mlist[16].hide();

	return ( is_writing=true );
}
//--------------------------------------------------------------------------
void Channel::stopWriting()
{
	is_writing = false;
	Gtk::Menu::MenuList& mlist = m_Menu_Popup.items();
	mlist[2].set_label(START_WRITING_CHANNEL_TEXT);
	mlist[13].show();
	mlist[14].show();
	mlist[15].show();
	mlist[16].show();
}
//--------------------------------------------------------------------------
void Channel::set_rewrite(bool state)
{
	rewrite = state;
	Gtk::Menu::MenuList& mlist = m_Menu_Popup.items();

	if(rewrite)
		mlist[14].set_label(TURN_OFF_CYCLE_TEXT);
	else
		mlist[14].set_label(TURN_ON_CYCLE_TEXT);
}
//--------------------------------------------------------------------------
void Channel::write_to_stringstream(stringstream& s, double x, double y, int hour, int min, int sec)
{
	s<<"\n\""<<hour<<":"<<setw(2)<<setfill('0')<<min<<":"<<setw(2)<<setfill('0')<<sec;
	s<<"\"";
	s<<",\""<<(long)x<<","<<setw(5)<<setfill('0')<<(long)((x-(long)x)*100000);
	s<<"\"";
	s<<",\""<< (y>=0? "":"-") <<abs((long)y)<<","<<setw(5)<<setfill('0')<<abs((long)((y-(long)y)*100000));
	s<<"\"";
}
//--------------------------------------------------------------------------
void Channel::save_in_file( string filename, double x_m, double x_mx)
{
	if(x_m==0 && x_mx==0)
	{
		x_m = 0;//!history.empty()? history.begin()->get_x():0;
		x_mx = get_last_x();
	}

	cout<<"файл: "<<filename<<endl;
//	filename = Glib::convert(filename,to_CodePage,from_CodePage);

	cout<<"попытка записи в файл: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cerr << "НЕ СМОГ ОТКРЫТЬ ФАЙЛ НА ЗАПИСЬ " << filename << endl;
		return;
	}

	History tmpHistory;
	stringstream s;
	#warning problems with system symbols in label name when trying to open it in oocalc or excel
	s<<"Время,\"Отсчёт,[сек]\",\""<<(get_button()->get_label())<<"\"";

	goHistoryHead();
	while( readNextHistory(tmpHistory) )
	{
		double x	= tmpHistory.x;
		if( x < x_m && x > x_mx )
			continue;
		double y	= tmpHistory.y;
		int hour	= tmpHistory.time.hour;
		int min		= tmpHistory.time.min;
		int sec		= tmpHistory.time.sec;

		write_to_stringstream(s,x,y,hour,min,sec);
	}
	for(std::list<History>::iterator it = get_history().begin(); it!=get_history().end(); ++it )
	{
		double x	= (*it).x;
		if( x < x_m && x > x_mx )
			continue;
		double y	= (*it).y;
		int hour	= (*it).time.hour;
		int min		= (*it).time.min;
		int sec		= (*it).time.sec;

		write_to_stringstream(s,x,y,hour,min,sec);
	}

	// save current data
	for( std::list<PlotMM::DoublePoint>::iterator it = get_data().begin(); it!=get_data().end(); ++it )
	{
		if( it->get_x() >= x_m && it->get_x() <= x_mx )
		{
			double x=it->get_x();
			int hour;
			int min;
			int sec;
			hour	= get_dataT()[x].hour;
			min	= get_dataT()[x].min;
			sec	= get_dataT()[x].sec;
//			timeToInt((long)x,hour,min,sec);
			double y=it->get_y();

			write_to_stringstream(s,x,y,hour,min,sec);
		}
	}

	fout<<Glib::convert((s.str()),"UNICODE","UTF8");//conversion from utf8 used here to utf16 (only encoding supported in ms office)
	fout.close();
}
//--------------------------------------------------------------------------
void Channel::save_channel_with_save_dialog(string name, string myname, string savefolder)
{
	//	enable
	if( get_data().empty() )
	{
		Gtk::MessageDialog mdlg( get_button()->get_label() + (": Нет данных.") ,true,Gtk::MESSAGE_WARNING);
		mdlg.run();
		return;
	}

	Gtk::FileChooserDialog dlg("Save data file...", Gtk::FILE_CHOOSER_ACTION_SAVE );

//	if(mwin)
//		dlg.set_parent(*mwin);

//	dlg.hide_fileop_buttons();
	dlg.set_modal(true);
	dlg.set_position(Gtk::WIN_POS_CENTER);
//	dlg.maximize();
//	dlg.set_decorated(false);
	dlg.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	dlg.add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);

//	---------- Облать ввода	диапазона ---------
	Gtk::VBox* vbox = dlg.get_vbox();
	Gtk::HBox hb;
	Gtk::SpinButton sp_min;
	Gtk::SpinButton sp_max;
	
	double x_m = 0;//!history.empty()? history.begin()->get_x():0;
	double x_mx =  get_last_x();
	
	sp_min.set_range(x_m,get_last_x()-1);
	sp_max.set_range(x_m+1,get_last_x());
	sp_min.set_increments(0.1,10);
	sp_min.set_digits(2);
	sp_min.set_numeric(true);
	sp_max.set_increments(0.1,10);
	sp_max.set_digits(2);
	sp_max.set_numeric(true);

	sp_min.set_value(x_m);
	sp_max.set_value(get_last_x());
//	sp_max.set_value( curve->max_x_value() );
	sp_min.show();
	sp_max.show();

	Gtk::Label l1( ("Сохраняемый интервал времени c") );
	Gtk::Label l2( ("по") );
	Gtk::Label l3( ("[ сек ]") );
	l1.show();
	l2.show();
	l3.show();
	
	hb.pack_start(l1, Gtk::PACK_SHRINK,5);
	hb.pack_start(sp_min, Gtk::PACK_SHRINK,5);
	hb.pack_start(l2, Gtk::PACK_SHRINK,5);
	hb.pack_start(sp_max, Gtk::PACK_SHRINK,5);
	hb.pack_start(l3, Gtk::PACK_SHRINK,5);

	vbox->pack_end(hb, Gtk::PACK_SHRINK,5);
	hb.show();
//	vbox->show();
//	-------------------------------------------

	Gtk::FileFilter fcsv;
	fcsv.set_name("(CSV) Comma separated text files");
	fcsv.add_mime_type("text/x-comma-separated-values");
//	fcsv_text.add_pattern("*.csv");
	dlg.add_filter(fcsv);

	Gtk::FileFilter fany;
	fany.set_name("Any files");
	fany.add_pattern("*");
	dlg.add_filter(fany);

//	cout << "save to file: " << dlg.get_current_folder() << "/" << btn->get_label() << ".csv" << endl;
	dlg.set_current_name( dateToString(time(0),"") + "_" + timeToString(time(0),"") + "_" + ((name.empty()? myname:name).c_str()) + "_" + get_button()->get_label() + ".csv" );

	dlg.set_current_folder(savefolder);
	dlg.set_position(Gtk::WIN_POS_CENTER);
	int result = dlg.run();

	switch(result)
	{
		case(Gtk::RESPONSE_OK):
		{
			dlg.set_current_name( Channel::dateToString(time(0),"")+ "_" + Channel::timeToString(time(0),"")+ "_" + get_button()->get_label() + ".csv" );
			if( dlg.get_filename().empty() )
				return;
//			x_m = sp_min.get_value();
//			x_mx = sp_max.get_value();
			if( x_m > x_mx )
			{
				x_m = sp_max.get_value();
				x_mx = sp_min.get_value();
			}

			save_in_file( dlg.get_filename(),x_m,x_mx);
		}
		break;

		case(Gtk::RESPONSE_CANCEL):
			break;
	
		default:
			break;
	}
}
//--------------------------------------------------------------------------
