//$Id: Dialogs.cc,v 1.1 2006/11/01 14:40:47 pv Exp $
//--------------------------------------------------------------------------
#include <sstream>
#include <iostream>
#include "Dialogs.h"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
InputValueDialog::InputValueDialog( const std::string tit )
{
	d.set_title( (tit) );
	d.add_button (Gtk::Stock::OK,Gtk::RESPONSE_OK);
	d.add_button (Gtk::Stock::CANCEL,Gtk::RESPONSE_CANCEL);

	Gtk::VBox* vbox = d.get_vbox();
	
	Gtk::HBox* hbox = manage( new Gtk::HBox() );
	hbox->pack_start(sp, Gtk::PACK_EXPAND_WIDGET,2);
	hbox->pack_start(l_unit, Gtk::PACK_SHRINK,5);
	
	vbox->pack_start(l, Gtk::PACK_SHRINK,5);
	vbox->pack_start(*hbox,Gtk::PACK_SHRINK,2);
	vbox->show_all_children(true);
}

//--------------------------------------------------------------------------
InputValueDialog::~InputValueDialog()
{

}
//--------------------------------------------------------------------------
void InputValueDialog::set_increments( double step, double page, unsigned int digits )
{
	sp.set_increments(step,page);
	sp.set_digits(digits);
}
//--------------------------------------------------------------------------
double InputValueDialog::run( double defval, double min, double max, const std::string unit )
{
	sp.set_range(min,max);
	sp.set_numeric(true);
	sp.set_value( defval );

	ostringstream s;
	s << "Введите число в диапазоне \nот " << min << " до " << max;
	l.set_text( (s.str()) );
	l_unit.set_text( (" [ " + unit + " ]") );
	if( d.run() == Gtk::RESPONSE_OK )
		return sp.get_value();

	return defval;
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
ConnectedValueDialog::ConnectedValueDialog( const std::string title, double rel2, double rel3 )
{
	d.set_title( (title) );
	d.add_button (Gtk::Stock::OK,Gtk::RESPONSE_OK);
	d.add_button (Gtk::Stock::CANCEL,Gtk::RESPONSE_CANCEL);

	sp1.signal_value_changed().connect(sigc::mem_fun(this, &ConnectedValueDialog::on_my_value1_changed));
	sp2.signal_value_changed().connect(sigc::mem_fun(this, &ConnectedValueDialog::on_my_value2_changed));
	sp3.signal_value_changed().connect(sigc::mem_fun(this, &ConnectedValueDialog::on_my_value3_changed));

	relation2 = rel2;
	relation3 = rel3;

	Gtk::VBox* vbox = d.get_vbox();

	Gtk::HBox* hbox = manage( new Gtk::HBox() );
	Gtk::VBox *vbox1, *vbox2, *vbox3;
	vbox1 = manage( new Gtk::VBox() );
	vbox2 = manage( new Gtk::VBox() );
	vbox3 = manage( new Gtk::VBox() );

	vbox1->pack_start(sp1, Gtk::PACK_SHRINK,2);
	vbox1->pack_start(l_unit1, Gtk::PACK_SHRINK,5);
	hbox->pack_start(*vbox1, Gtk::PACK_SHRINK,2);

	vbox2->pack_start(sp2, Gtk::PACK_SHRINK,2);
	vbox2->pack_start(l_unit2, Gtk::PACK_SHRINK,5);
	hbox->pack_start(*vbox2, Gtk::PACK_SHRINK,2);

	vbox3->pack_start(sp3, Gtk::PACK_SHRINK,2);
	vbox3->pack_start(l_unit3, Gtk::PACK_SHRINK,5);
	hbox->pack_start(*vbox3, Gtk::PACK_SHRINK,2);

	vbox->pack_start(l, Gtk::PACK_SHRINK,5);
	vbox->pack_start(*hbox,Gtk::PACK_SHRINK,2);
	vbox->show_all_children(true);
}

//--------------------------------------------------------------------------
ConnectedValueDialog::~ConnectedValueDialog()
{
}
//--------------------------------------------------------------------------
void ConnectedValueDialog::set_increments(double step, double page, unsigned int digits)
{
	sp1.set_increments(step, page);
	sp1.set_digits(digits);

	sp2.set_increments(step, page);
	sp2.set_digits(digits);

	sp3.set_increments(step, page);
	sp3.set_digits(digits);
}
//--------------------------------------------------------------------------
void ConnectedValueDialog::set_labels(const std::string unit1, const std::string unit2, const std::string unit3)
{
	l_unit1.set_text( (" [ " + unit1 + " ]") );
	l_unit2.set_text( (" [ " + unit2 + " ]") );
	l_unit3.set_text( (" [ " + unit3 + " ]") );
}
double ConnectedValueDialog::run( double defval, double min, double max)
{
	sp1.set_range(min, max);
	sp1.set_numeric(true);
	sp1.set_value(defval);

	sp2.set_range(min*relation2, max*relation2);
	sp2.set_numeric(true);
	sp2.set_value(defval*relation2);

	sp3.set_range(min*relation3, max*relation3);
	sp3.set_numeric(true);
	sp3.set_value(defval*relation3);

	ostringstream s;
	s << "Введите число в диапазоне \nот " << min << " до " << max;
	l.set_text( (s.str()) );
	if( d.run() == Gtk::RESPONSE_OK )
		return sp1.get_value();

	return defval;
}
//--------------------------------------------------------------------------
void ConnectedValueDialog::on_my_value1_changed()
{
	sp2.set_value(sp1.get_value()*relation2);
	sp3.set_value(sp1.get_value()*relation3);
}
//--------------------------------------------------------------------------
void ConnectedValueDialog::on_my_value2_changed()
{
	sp1.set_value(sp2.get_value()/relation2);
	sp3.set_value(sp1.get_value()*relation3);
}
//--------------------------------------------------------------------------
void ConnectedValueDialog::on_my_value3_changed()
{
	sp1.set_value(sp3.get_value()/relation3);
	sp2.set_value(sp1.get_value()*relation2);
}
//--------------------------------------------------------------------------