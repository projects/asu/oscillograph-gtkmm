//$Id: Dialogs.h,v 1.2 2009/06/22 12:46:44 alex Exp $
// -----------------------------------------------------------------------------
#ifndef _DIALOG_H
#define _DIALOG_H
// -----------------------------------------------------------------------------
#include <gtkmm.h>
// -----------------------------------------------------------------------------
class InputValueDialog
{
	public:
		InputValueDialog( const std::string title );
		~InputValueDialog();

		double run( double defvalue, double min, double max, const std::string unit="" );

		void set_increments( double step, double page, unsigned int digits );
		inline Gtk::Dialog* get_dialog() { return &d; }
	
	protected:
		Gtk::Dialog d;
		Gtk::Label l;
		Gtk::Label l_unit;
		Gtk::SpinButton sp;

	private:
};

class ConnectedValueDialog
{
	public:
		ConnectedValueDialog(const std::string title, double rel1, double rel2);
		~ConnectedValueDialog();

		double run(double defvalue, double min, double max);

		void set_increments(double step, double page, unsigned int digits);
		void set_labels(const std::string unit1="", const std::string unit2="", const std::string unit3="");

		inline Gtk::Dialog* get_dialog() {return &d;}

	protected:
		Gtk::Dialog d;
		Gtk::Label l;
		Gtk::Label l_unit1, l_unit2, l_unit3;
		Gtk::SpinButton sp1, sp2, sp3;
		double relation2, relation3;

	private:
		void on_my_value1_changed();
		void on_my_value2_changed();
		void on_my_value3_changed();
};
// -----------------------------------------------------------------------------
#endif // _DIALOG_H
// -----------------------------------------------------------------------------
