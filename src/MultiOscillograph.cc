//$Id: MultiOscillograph.cc,v 1.3 2006/11/21 15:49:25 pv Exp $
//--------------------------------------------------------------------------
#include <iostream>
#include "MultiOscillograph.h"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
MultiOscillograph::MultiOscillograph( int onum, int up_msec ):
	olst(onum),
	t_step( (double)up_msec / 1000.0 ),
	t(0),
	tmr_msec(up_msec)
{
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
	{
		(*it) = new Oscillograph(0,0);
//		pack_start( *(*it),Gtk::PACK_SHRINK,0);
		pack_start( *(*it),Gtk::PACK_EXPAND_WIDGET,0);
		(*it)->signal_set_timer_step().connect(sigc::mem_fun(*this,&MultiOscillograph::update_timer_time));
		(*it)->show();
	}
	
	show_all_children(true);
	show();
}
//--------------------------------------------------------------------------
MultiOscillograph::~MultiOscillograph()
{
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
		delete (*it);
}
//--------------------------------------------------------------------------
Oscillograph* MultiOscillograph::get_oscillograph( int num )
{
	if( num < 1 || num > (int)olst.size() )
		return 0;
	
	return olst[num-1];
}
//--------------------------------------------------------------------------
Oscillograph* MultiOscillograph::add_oscillograph( bool pack )
{
	int ind = olst.size();
	olst.resize( ind+1 );
	
	Oscillograph* osc = new Oscillograph(0,0);
	
	if( pack )
	{
	//	pack_start( *osc,Gtk::PACK_SHRINK,0 );
		pack_start( *osc,Gtk::PACK_EXPAND_WIDGET,0 );
	}
	
	osc->signal_set_timer_step().connect(sigc::mem_fun(*this,&MultiOscillograph::update_timer_time));
	osc->show();
	olst[ind] = osc;
	return osc;
}
//--------------------------------------------------------------------------
int MultiOscillograph::get_number( Oscillograph* osc )
{
	int i=1;
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
	{
		if( (*it) == osc )
			return i;
		i++;
	}
	return 0;
}
//--------------------------------------------------------------------------
bool MultiOscillograph::onUpdateTime()
{
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
	{
		if( (*it)->enabled_timer() )
			(*it)->tick(t);
	}
	
	t +=t_step;	
	return true;
}
//--------------------------------------------------------------------------
void MultiOscillograph::set_timer_step( int msec )
{
	if( tmr_msec!= msec )
	
	tmr_msec = msec;
	if(tmr)
		tmr.disconnect();

	if( tmr_msec )
	{	
		t_step = (double)tmr_msec / 1000.0;
	
		tmr = Glib::signal_timeout().connect(
					sigc::mem_fun(*this, &MultiOscillograph::onUpdateTime), tmr_msec);
	}


	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
		(*it)->set_timer_step(tmr_msec);
}
//--------------------------------------------------------------------------
void MultiOscillograph::update_timer_time( int old_t, int new_t, Oscillograph* o )
{
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
	{
		if( (*it)!=o )
			(*it)->set_timer_step( new_t );
	}
}
//--------------------------------------------------------------------------
void MultiOscillograph::update_timer_interval( double old_i, double new_i, Oscillograph* o )
{
/*
	cout << "update_timer_interval: new_t=" << old_t << " new_t=" << new_i << endl;
	for( OscilList::iterator it=olst.begin(); it!=olst.end(); ++it )
	{
		if( (*it)!=o )
			(*it)->set_time_interval( new_i );
	}
*/	
}
//--------------------------------------------------------------------------
