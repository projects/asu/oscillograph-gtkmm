//$Id: Axis.cc,v 1.3 2006/11/28 10:23:32 pv Exp $
//--------------------------------------------------------------------------
#include <iostream>
#include "Axis.h"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
Axis::Axis(): 
	enable(true),
	data(2),
	clr(Gdk::Color("gray"))
{
	init();
}
//--------------------------------------------------------------------------
void Axis::init()
{
	curve = Glib::RefPtr<PlotMM::Curve>( new PlotMM::Curve("Axis") );
	curve->set_curve_style( PlotMM::CURVE_LINES );
}

//--------------------------------------------------------------------------
Axis::~Axis()
{

}
//--------------------------------------------------------------------------
void Axis::set_enabled( bool st )
{
	if( enable==st )
		return;

	enable = st;
	curve->set_enabled(st);
	
	if( enable )
		replot();
}
//--------------------------------------------------------------------------
void Axis::replot()
{
	curve->set_data(data);
}
//--------------------------------------------------------------------------
void Axis::set_color( Gdk::Color _clr )
{
	clr = _clr;
	curve->paint()->set_pen_color(clr);
}
//--------------------------------------------------------------------------
VAxis::VAxis()
{

}
//--------------------------------------------------------------------------
VAxis::~VAxis()
{

}
//--------------------------------------------------------------------------
void VAxis::draw( double x )
{
	data[0].set_x(x); // y0
	data[1].set_x(x); // y1
}
//--------------------------------------------------------------------------
void VAxis::set_length( double ymin, double ymax )
{
	data[0].set_y(ymin); // y0
	data[1].set_y(ymax); // y1
}
//--------------------------------------------------------------------------
HAxis::HAxis()
{

}
//--------------------------------------------------------------------------
HAxis::~HAxis()
{

}
//--------------------------------------------------------------------------
void HAxis::draw( double y )
{
	data[0].set_y(y); // y0
	data[1].set_y(y); // y1
}
//--------------------------------------------------------------------------
void HAxis::set_length( double xmin, double xmax )
{
	data[0].set_x(xmin); // x0
	data[1].set_x(xmax); // x1
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
Grid::Grid()
{

}
//--------------------------------------------------------------------------
Grid::~Grid()
{

}
//--------------------------------------------------------------------------
void Grid::draw( double xmin, double xmax, double ymin, double ymax )
{

}
//--------------------------------------------------------------------------
void Grid::replot()
{

}
//--------------------------------------------------------------------------
void Grid::set_enabled( bool st )
{

}
//--------------------------------------------------------------------------
void Grid::set_color( Gdk::Color clr )
{

}
//--------------------------------------------------------------------------
void Grid::init()
{

}
//--------------------------------------------------------------------------
