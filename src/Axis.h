//$Id: Axis.h,v 1.3 2008/01/23 13:17:06 pv Exp $
// -----------------------------------------------------------------------------
#ifndef _AXIS_H
#define _AXIS_H
// -----------------------------------------------------------------------------
#include <gtkmm.h>
#include <vector>
#include <plotmm/paint.h>
#include <plotmm/curve.h>
// -----------------------------------------------------------------------------
class Axis
{
	public:
		Axis();
		virtual ~Axis();

		// plot
		virtual void draw( double val ) = 0;
		virtual void set_length( double min, double max ) = 0;

		void replot();

		void set_enabled( bool st );
		inline bool enabled(){ return enable; }
	
		void set_color( Gdk::Color clr );

		inline bool empty(){ return data.empty();}

		inline Glib::RefPtr<PlotMM::Curve> get_curve() { return curve; } 

	protected:
		void init();

		bool enable;
		std::vector<PlotMM::DoublePoint> data;
		Glib::RefPtr<PlotMM::Curve> curve;
		Gdk::Color clr;
};
// -----------------------------------------------------------------------------
class VAxis:
	public Axis
{
	public:
		VAxis();
		virtual ~VAxis();

		// plot
		virtual void draw( double x );
		virtual void set_length( double y_min, double y_max );

	protected:
	private:
};
// -----------------------------------------------------------------------------
class HAxis:
	public Axis
{
	public:
		HAxis();
		virtual ~HAxis();

		// plot
		virtual void draw( double y );
		virtual void set_length( double x_min, double x_max );

	protected:
	private:
};
// -----------------------------------------------------------------------------
class Grid
{
	public:
		Grid();
		~Grid();

		void draw( double xmin, double xmax, double ymin, double ymax );
		void set_ticks_count( double x_ticks=10, double y_ticks=10 );

		void replot();

		void set_enabled( bool st );
		inline bool enabled(){ return enable; }
	
		void set_color( Gdk::Color clr );

		inline bool empty(){ return data.empty();}

		inline Glib::RefPtr<PlotMM::Curve> get_curve() { return curve; } 

	protected:
		void init();

		bool enable;
		std::vector<PlotMM::DoublePoint> data;
		Glib::RefPtr<PlotMM::Curve> curve;
		Gdk::Color clr;
};
// -----------------------------------------------------------------------------
#endif // _AXIS_H
// -----------------------------------------------------------------------------
