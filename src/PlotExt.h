#include <plotmm/plot.h>

//using namespace PlotMM;

class PlotExt :
	public PlotMM::Plot
{
public:
	PlotExt():
		PlotMM::Plot()
	{	}
	virtual ~PlotExt()
	{	}
	inline int get_canvas_width()
	{ return canvas_.get_width(); }

	inline int get_canvas_height()
	{ return canvas_.get_height(); }

	inline void set_canvas_tootip_text(Glib::ustring text)
	{ canvas_.set_tooltip_text(text); }
};
