//$Id: StorInterface.h, 2010/06/11 13:25:08 Ivan Exp $
#ifndef _StorInterface_H
#define _StorInterface_H

#include <string>

#warning значение размера элемента данных нужно задать!Здесь заглушка!
#define StorageElem 1

#warning значение размера заголовка нужно задать!Здесь заглушка!
#define StorageAttr 1

struct Time
{
	Time():
		hour(0),
		min(0),
		sec(0)
		{}
	int hour;
	int min;
	int sec;
}__attribute__((packed));

struct History
{
	History():
		time(),
		x(0),
		y(0)
		{}
	Time time;
	double x;
	double y;
}__attribute__((packed));

class StorInterface
{
	public:
		inline int getHead(){ return -1; }
		inline int getTail(){ return -1; }

		StorInterface() { item_size = storage_size = 0; };

		StorInterface(const std::string name, int max_history_size, int hist_type_size, bool cr=false)
		{
			item_size = hist_type_size + StorageElem;
			storage_size = StorageAttr + max_history_size * item_size;
		}

		bool open( const std::string name, int max_history_size, int hist_type_size );
		bool create( const std::string name, int max_history_size, int hist_type_size );

		bool isOpen();
		bool isFull();

		bool write( History& hist );
		bool read( int num, History& hist );//num - номер от начала, начинается с 0

		inline int get_storage_size() { return storage_size; }
		inline int get_header_size() { return StorageAttr; }
		inline int get_elem_header_size() { return StorageElem; }
		inline int get_item_size() { return item_size; }
	private:
		int storage_size, item_size;
};
// -------------------------------------------------------------------------
#endif // _StorInterface_H
// -------------------------------------------------------------------------