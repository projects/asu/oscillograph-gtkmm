//$Id: Oscillograph.h,v 1.22 2010/02/04 08:08:59 alex Exp $
// -----------------------------------------------------------------------------
#ifndef _OSCILLOGRAPH_H
#define _OSCILLOGRAPH_H
// -----------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include <gtkmm.h>
#include <vector>
#include <plotmm/curve.h>
#include <plotmm/paint.h>
#include <plotmm/rectangle.h>
#include <PlotExt.h>
#include "Channel.h"
#include "Axis.h"

static const std::string START_WRITING_ALL_CHANNELS_TEXT = "Начать запись всех каналов";
static const std::string STOP_WRITING_ALL_CHANNELS_TEXT = "Остановить запись всех каналов";
static const std::string SAVE_TO_FILE_TEXT = "Сохранить данные в файл...";
static const std::string SAVE_ALL_TO_FILES_TEXT = "Сохранить данные в файл... (для всех включённых каналов)";
static const std::string SAVE_ALL_TO_ONE_FILE_TEXT = "Сохранить в один файл все включённые каналы";
static const std::string TURN_ON_ALL_CHANNELS_TEXT = "Включить все каналы";
static const std::string TURN_OFF_ALL_CHANNELS_TEXT = "Отключить все каналы";
static const std::string DELETE_CHANNEL_TEXT = "Удалить";
static const std::string SET_TEMP_FILE_TEXT = "Задать размер временного файла";
static const std::string TURN_OFF_ALL_CYCLE_TEXT = "Отключить циклическую перезапись для всех каналов";
static const std::string TURN_ON_ALL_CYCLE_TEXT = "Включить циклическую перезапись для всех каналов";

// -----------------------------------------------------------------------------
class Oscillograph:
	public Gtk::Frame
{
    public:
        Oscillograph( int numChannels=0, int up_msec=100, const std::string nm="", const std::string sf="//");
		Oscillograph( bool cycle_on_off, bool write_on_off, int numChannels=0, int up_msec=100, const std::string nm="", const std::string sf="//");
        virtual ~Oscillograph();

		void init( int numChannels=0, int up_msec=100, const std::string nm="", const std::string sf="//");
		// Plot params
		void set_plot_size( int width, int height );
		void set_plot_title( const std::string title );
		void set_axis_label( PlotMM::PlotAxisID id, const Glib::ustring lbl  );
		void set_y_range(double y_min,double y_max);
		void channel_toggled(int numOwnNotebook,int numCurrentNotebook);
		void unzoom();
		
		// design
		void set_legend_width( int w );
		// disable labels and quest button
		void disable_auxlabels( bool sw );

		// timer settings
		inline double get_time_elapsed() { return t; }
		void set_time_interval( double sec );
		void set_timer_step( int msec );
		inline double get_timer_step() { return t_step; }
		bool tick( const double t );

		void set_cycle_rewrite_all(bool state);
		inline bool get_cycle_rewrite()
		{ return cycle_save_all_channels; }

		// запустить собственный таймер
		void run();
		// остановить собственный таймер
		void stop();

		// void set_timer_step( int old_msec, int new_msec );
		typedef sigc::signal<void,int,int,Oscillograph*> SetTimeStep_Signal;
		SetTimeStep_Signal signal_set_timer_step();
		//инициируется при получении события клика по меню "удалить канал". передает номер канала в осциллографе
		typedef sigc::signal<void, int, Oscillograph*> signal_ChannelRemove;
		signal_ChannelRemove signal_channel_remove();
	
		inline Glib::RefPtr<Channel> get_channel( int num )
		{ return get_channel_info(num).c;	}
		void plot( int channel, double x, double y );

		Glib::RefPtr<Channel> add_channel( const Glib::ustring lbl, Gdk::Color clr=Gdk::Color("black"), std::string filename="",std::string tmpdir="", int size=100, int filesz=5000);
		Glib::RefPtr<Channel> add_channel( const Glib::ustring lbl, Gdk::Color clr=Gdk::Color("black"), int size=100, int filesz=5000, std::string filename="", std::string tmpdir="" );

		int get_number( Glib::RefPtr<Channel>& c );
		inline int count(){ return clst.size(); }
		void remove_channel(Glib::RefPtr<Channel>& c);
		void remove_channel(int cnum);

		// control		

		void set_enable_timer( bool st );
		inline bool enabled_timer() { return tmr_enabled; }
		inline Gtk::CheckButton* get_btn_tmr() { return &btn_tmr; }
		// сохранять предыдущее значение при отсутсвии сигнала
		void set_backtrace_enable( bool st );
		inline bool backtrace_enabled(){ return backtrace; }

		// data buffer size for all channels [ kbytes ]
		unsigned int all_history_max_kbytes();

		void set_enable_x_axis( bool state );
		inline bool x_axis_enabled(){ return mi_x_axis->get_active(); }

		void set_enable_runner( bool state );
		inline bool runner_enabled(){ return mi_runner->get_active(); }

		inline Gtk::Frame* get_b_frm(){return b_frm;}
		inline Gtk::Table* get_b_tbl(){return &m_tbl;}
		inline Gtk::Viewport* get_b_vport(){ return b_vport; }
		
		inline Gtk::Label*  get_btn_tmr_label(){ return btn_tmr_label;}
		inline Gtk::Label*  get_m_sb_label(){ return m_sb_label;}
		inline Gtk::ScrolledWindow*  get_scwin(){ return scwin;}

		// menus
		inline bool menu_runner()
			{ return mi_runner->is_sensitive(); }

		inline bool menu_x_axis()
			{ return mi_x_axis->is_sensitive(); }

		inline bool menu_backtrace()
			{ return mi_backtrace->is_sensitive(); }

		inline bool menu_t_interval()
			{ return mi_t_interval->is_sensitive(); }

		inline bool menu_t_step()
			{ return mi_t_step->is_sensitive(); }
		
		inline bool menu_x_step()
			{ return mi_x_step->is_sensitive(); }

		void menu_runner_set_sensitive( bool state );
		void menu_x_axis_set_sensitive( bool state );
		void menu_backtrace_set_sensitive( bool state );
		void menu_t_interval_set_sensitive( bool state );
		void menu_t_step_set_sensitive( bool state );
		void menu_x_step_set_sensitive( bool state );

		PlotMM::Plot* get_plot(){ return &m_plot; }

		inline std::string get_name(){ return name; }
		inline void set_name(std::string str){ name = str; }

		inline std::string get_myname(){ return myname; }
		inline void set_myname(std::string str){ myname = str; }

		void timeToInt(long time,int &hour, int &min, int &sec); /*!< Преобразование времени в числа*/

		inline void set_save_folder( std::string sf )
		{ savefolder = sf; }

		//"занят" ли этот цвет. (возможно, на осциллографе уже есть канал данного цвета
		bool whether_color_is_taken(Gdk::Color*);

		void save_channel_with_save_dialog(Glib::RefPtr<Channel>& c);/*! Для прямого вызова из внешних классов */
		void save_all_channels();
		void save_all_channels_in_one();

		//права доступа к параметру максимального расстояния от курсора до кривой
		//при сохранении возможности появления подсказки.
		inline void set_y_tolerance(int iTolerance)
		{ y_tolerance= (iTolerance>0) ? iTolerance: 25; }
		inline int get_y_tolerance()
		{ return y_tolerance;	}

		inline float get_approaching_coefficient()
		{ return approaching_coefficient; }
		inline void set_approaching_coefficient(const float iCoeff)
		{ approaching_coefficient = ((iCoeff>0) && (iCoeff<1)) ? iCoeff : 0.8; }

		inline int get_color_tolerance()
		{ return color_tolerance; }
		inline void set_color_tolerance(int icolor_tolerance)
		{ color_tolerance = (icolor_tolerance>0 && icolor_tolerance<256) ?  icolor_tolerance : 20; }

		void set_write_all_channels(bool state);
		Gtk::Menu* get_menu(){return &m_Menu_Popup;}

		void set_mouse_is_pressed(bool val){ mouse_is_pressed = val; }
		bool get_mouse_is_pressed(){ return mouse_is_pressed; }

		sigc::connection plot_press;
		sigc::connection plot_release;
    protected:
		typedef std::vector<std::string> Line;
		typedef std::map<double,Line> Matrix;

		signal_ChannelRemove s_ch_remove;
		
		std::string name;

		void print_coords(int x, int y);
		void on_plot_mouse_move(int x,int y, GdkEventMotion *ev);
		void on_plot_mouse_tooltip_move(int x,int y, GdkEventMotion *ev);
		void on_plot_mouse_press(int x,int y, GdkEventButton *ev);
		void on_plot_mouse_release(int x,int y, GdkEventButton *ev);
		void on_but_timer_toggled();
		void on_channel_btn_toggled();
		void on_quest_button_toggled();
		
		// popup menu
		void on_popup_set_interval();
		void on_popup_set_t_step();
		void on_popup_set_x_step();
		void on_popup_stop();
		void on_popup_start();
		void on_popup_unzoom();
		void on_popup_runner();
		void on_popup_x_axis();
		void on_popup_backtrace();
		void on_menu_save_file(Glib::RefPtr<Channel>& c);
		void on_menu_toggle_writing(Glib::RefPtr<Channel>& c);
		void on_menu_toggle_writing_all();
		void off_on_all_channels(bool mark);
		void off_on_cycle(Glib::RefPtr<Channel>& c);
		void set_cycle(Glib::RefPtr<Channel>& c, bool state);
		void off_on_cycle_all();
		void on_menu_remove_channel(Glib::RefPtr<Channel>& c);
		void on_menu_set_file_history_max_point(Glib::RefPtr<Channel>& c);
		bool on_plot_scroll_event(GdkEventScroll* event);

		void save_in_file( Glib::RefPtr<Channel>& c, std::string filename);

		//this two functions remove some blocks of the same code
		void write_to_stringstreams(stringstream& stime, stringstream& sy, double x, double y, int hour, int min, int sec);
		Line create_file_line(double x, double y, int hour, int min, int sec, unsigned int cnum);

		bool onUpdateTime();
		void set_self_timer( bool st );
		//!NB There is the changing!

		typedef Glib::RefPtr<Channel>		CPtr;
		class ChannelInfo
		{
		public:
			Glib::RefPtr<Channel>		c;
			//хранит подписку на событие клика по выпадающему меню "сохранить_канал"
			sigc::connection 		save_data_connection;
			//хранит подписку на событие клика по выпадающему меню "удалить_канал"
			sigc::connection 		remove_channel_connection;

			sigc::connection		toggle_writing_connection;
			inline int operator ==(const ChannelInfo& iOther) const
			{
				return iOther.c==c;
			}
			inline int operator !=(const ChannelInfo& iOther) const
			{
				return iOther.c!=c;
			}
			ChannelInfo(Glib::RefPtr<Channel>& ch)
			{
				c=ch;
			}
			ChannelInfo()
			{	}
			ChannelInfo(const ChannelInfo& iOther)
			{
				c=iOther.c;
				save_data_connection =iOther.save_data_connection;
				remove_channel_connection =iOther.remove_channel_connection;
				toggle_writing_connection = iOther.toggle_writing_connection;
			}
			~ChannelInfo()
			{	}
			
			inline operator bool() // проверка инициализирована ли структура
			{
				return (bool)c;
			}
		};


		ChannelInfo get_channel_info( Glib::RefPtr<Channel>& c );
		ChannelInfo get_channel_info( int num );
		void init_channel( ChannelInfo& cinfo );

		VAxis rn;
		HAxis x_axis;

		typedef std::vector<ChannelInfo> ChannelList;
		ChannelList clst;
		sigc::connection tmr;
		sigc::connection mouse_tooltip_move;
		int tmr_msec;
		bool tmr_enabled;
		bool show_oscil;
		bool self_timer;
		bool backtrace;
		SetTimeStep_Signal m_timestep_signal;

		int mx_,my_;
		PlotMM::Rectangle zoomRect_;
		bool is_zooming;

		Gtk::HBox m_box0;
		Gtk::Table m_tbl;
		Gtk::VBox m_box2;
		Gtk::Frame*  b_frm;
		Gtk::Viewport* b_vport;
		Gtk::ScrolledWindow* scwin;
		Gtk::ToggleButton* quest_button;
		Gtk::VBox plot_container;

		Gtk::HBox* s_box;
		Gtk::Statusbar m_sb;
		Gtk::Label*  m_sb_label;
		Gtk::CheckButton btn_tmr;
		Gtk::Label*  btn_tmr_label;

		Gtk::MenuItem* mi_unzoom;
		Gtk::MenuItem* mi_stop;
		Gtk::MenuItem* mi_start;
		Gtk::CheckMenuItem* mi_runner;
		Gtk::CheckMenuItem* mi_x_axis;
		Gtk::CheckMenuItem* mi_backtrace;
		Gtk::MenuItem* mi_t_interval;
		Gtk::MenuItem* mi_t_step;
		Gtk::MenuItem* mi_x_step;


		Gtk::MenuItem* mi_toggle_writing;
		Gtk::MenuItem* mi_toggle_writing_all;

		Gtk::MenuItem* mi_toggle_cycle;
		Gtk::MenuItem* mi_toggle_cycle_all;

		Gtk::MenuItem* mi_save;
		Gtk::MenuItem* mi_save_all;
		Gtk::MenuItem* mi_save_all_in_one;
		Gtk::MenuItem* mi_on_all;
		Gtk::MenuItem* mi_off_all;
		Gtk::MenuItem* mi_remove;
		Gtk::MenuItem* mi_set_file_history_max_point;

		struct PlotData
		{
			std::list<PlotMM::DoublePoint>::iterator end;
			std::list<PlotMM::DoublePoint>::iterator it;
			std::map<double,Time>* time;
		};
		struct PlotHistory
		{
			std::list<History>::iterator end;
			std::list<History>::iterator it;
		};
		

		int tm_hour;
		int tm_min;
		int tm_sec;
		//чем меньше, тем ближе нужно подвести курсор к кривой канала,
		//чтобы появилась подсказка.
		int y_tolerance;
		//коэффициент изменения масштаба на m_plot.  NB! must be from 0 to 1
		float approaching_coefficient;
		
		static int oscilnum;
		std::string myname;
		std::string sessionname;
		bool mouse_is_pressed;
		
		PlotExt m_plot;
		double x_min;	// минимальное текущее значение (времени)
		double x_max;	// максимальное текущее значение (времени)
		double y_min;	// минимальное текущее значение координаты Y
		double y_max;	// максимальное текущее значение координаты Y
		double t;		// текущее время [сек]
		double x_step;	// сдвиг при достижении края экрана [сек]
		double t_step;	// шаг времени [сек]
		double t_interval_sec; // видимый на экране интервал времени [сек]
		//коэффициент "близости" цветов по rgb-компонентам.
		int color_tolerance;
		
		Gtk::Menu m_Menu_Popup;
	private:
		std::string savefolder;
		bool write_all_channels;
		bool cycle_save_all_channels;
};
// -----------------------------------------------------------------------------
#endif // _OSCILLOGRAPH_H
// -----------------------------------------------------------------------------
