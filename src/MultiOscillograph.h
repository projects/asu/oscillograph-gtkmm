//$Id: MultiOscillograph.h,v 1.4 2009/06/16 14:35:33 alex Exp $
// -----------------------------------------------------------------------------
#ifndef _MULTIOSCILLOGRAPH_H
#define _MULTIOSCILLOGRAPH_H
// -----------------------------------------------------------------------------
#include <gtkmm.h>
#include <vector>
#include "Oscillograph.h"
// -----------------------------------------------------------------------------
class MultiOscillograph:
	public Gtk::VBox
{
    public:
        MultiOscillograph( int numOscil=0, int up_msec=0 );
        virtual ~MultiOscillograph();
		
		Oscillograph* get_oscillograph( int num );
		Oscillograph*  add_oscillograph( bool pack = true );
		int get_number( Oscillograph* osc );
		
		inline int count(){ return olst.size(); }
		
		void set_timer_step( int msec );
		
    protected:
		bool onUpdateTime();
		void update_timer_time( int old_t, int new_t, Oscillograph* o );
		void update_timer_interval( double old_i, double new_i, Oscillograph* o );
	
	
		typedef std::vector<Oscillograph*> OscilList;
		OscilList olst;
		double t_step;
		double t;
		sigc::connection tmr;
		int tmr_msec;
};
// -----------------------------------------------------------------------------
#endif // _MULTIOSCILLOGRAPH_H
// -----------------------------------------------------------------------------
