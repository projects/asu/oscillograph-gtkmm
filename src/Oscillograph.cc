//$Id: Oscillograph.cc,v 1.44 2010/02/04 08:08:59 alex Exp $
//--------------------------------------------------------------------------
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "Oscillograph.h"
#include "Dialogs.h"
#include "algorithm"
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
// чтобы потом легче было перевести делаем пока const
static const double MinTimer_msec = 10.0;
static const double MaxTimer_msec = 20000.0;
static const double MaxValue = 100000.0;
int Oscillograph::oscilnum = 0;
//--------------------------------------------------------------------------
void Oscillograph::init( int numch, int up_msec , const std::string nm, const std::string sf)
{
	name = "";
	tmr_enabled = false;
	show_oscil = true;
	self_timer = true;
	backtrace = false;
	tmr_msec = up_msec;
	mx_ = -1000;
	my_ = -1000;
	is_zooming = false;
	myname = nm;
	x_min = 0.0;
	x_max = 50.0;
	y_min = -1.0;
	y_max = 1.0;
	t = 0.0;
	x_step = 0.0;
	t_step = (double)up_msec / 1000.0;
	t_interval_sec = 10.0;
	savefolder = sf;
	write_all_channels = false;
	cycle_save_all_channels = false;

	y_tolerance=25;
	color_tolerance=20;
	mouse_is_pressed=false;
	approaching_coefficient=0.8;
	oscilnum++;
	if(myname.empty())
	{
		stringstream s;
		s << "Oscillograph" << setw(2) << setfill('0') << oscilnum;
		myname = s.str();
	}
	
	self_timer = ( up_msec <= 0 ) ? false : true;
	
	// выставляем по умолчанию (заодно пересчитывая всё остальное)
	set_time_interval(t_interval_sec);

	sessionname = Channel::dateToString(time(0),"")+"_"+Channel::timeToString(time(0),"")+"_";
	int i=0;
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		//(*it).c = new Channel(sessionname);
		Glib::RefPtr<Channel> c_tmp(new Channel(sessionname));
		it->c=c_tmp;
		init_channel( (*it) );
		ostringstream s;
		s << "channel" << i+1;
		(*it).c->set_title(s.str());
		i++;
	}
	
	// x-axis
	m_plot.add_curve(x_axis.get_curve());
	x_axis.set_length(x_min,x_max);
	x_axis.draw(0);
	x_axis.set_color( Gdk::Color("gray") );

	// runner
	m_plot.add_curve(rn.get_curve());
	rn.set_length(y_min,y_max);


	set_shadow_type(Gtk::SHADOW_NONE);

	b_frm = manage( new Gtk::Frame() );

	m_tbl.set_border_width(2);
	m_tbl.set_col_spacings(1);
	m_tbl.set_row_spacings(2);
	m_tbl.show();

	Gtk::Adjustment* v_adj = manage( new Gtk::Adjustment(0,0,0) );
	Gtk::Adjustment* h_adj = manage( new Gtk::Adjustment(0,0,0) );
	b_vport = manage( new Gtk::Viewport(*v_adj,*h_adj) );
	b_vport->set_shadow_type(Gtk::SHADOW_NONE);
	b_vport->add(m_tbl);
	
	scwin = manage( new Gtk::ScrolledWindow() );
	scwin->add(*b_vport);
	scwin->set_policy( Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC );
	scwin->set_shadow_type(Gtk::SHADOW_NONE);
	scwin->set_border_width(2);
	scwin->show();

	b_frm->add(*scwin);

	b_frm->set_label(("Каналы"));
	b_frm->set_shadow_type(Gtk::SHADOW_NONE);

	m_box0.pack_start(*b_frm, Gtk::PACK_SHRINK, 2);
	plot_container.pack_start(m_plot,Gtk::PACK_EXPAND_WIDGET, 0);
	plot_container.show();
	plot_container.signal_scroll_event().connect(sigc::mem_fun(*this, &Oscillograph::on_plot_scroll_event));
	m_box0.pack_start(plot_container, Gtk::PACK_EXPAND_WIDGET, 0);
	m_box0.show();

	m_plot.set_size_request(400,250);
	set_border_width(3);

	s_box = manage( new Gtk::HBox() );
//	btn_tmr.set_label("");
	btn_tmr_label = manage( new Gtk::Label() );
	m_sb_label = manage( new Gtk::Label() );
	m_sb_label->show();
	btn_tmr_label->show();
	btn_tmr.signal_toggled().connect(sigc::mem_fun(*this,&Oscillograph::on_but_timer_toggled));

	quest_button = manage( new Gtk::ToggleButton() );
	Gtk::Widget* image;
	image = manage (new Gtk::Image (Gtk::Stock::INFO, Gtk::ICON_SIZE_BUTTON));
	image->show();
	quest_button->add(*image);
	quest_button->signal_toggled().connect(sigc::mem_fun(*this,&Oscillograph::on_quest_button_toggled));
	quest_button->show();
//	m_sb.pack_start(btn_tmr, Gtk::PACK_SHRINK, 2);
	s_box->pack_end(btn_tmr, Gtk::PACK_SHRINK);
	s_box->pack_end(*btn_tmr_label, Gtk::PACK_SHRINK);
	s_box->pack_end(*quest_button, Gtk::PACK_SHRINK,3);
	s_box->pack_start(*m_sb_label);
	s_box->show();

	m_box2.pack_start(m_box0, Gtk::PACK_EXPAND_WIDGET, 2);
//	m_box2.pack_start(m_sb, Gtk::PACK_SHRINK, 0);
	m_box2.pack_start(*s_box, Gtk::PACK_SHRINK, 0);
	add(m_box2);
	show_all();

	m_plot.scale(PlotMM::AXIS_TOP)->set_enabled(false);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_enabled(true);
	m_plot.scale(PlotMM::AXIS_RIGHT)->set_enabled(false);
	m_plot.scale(PlotMM::AXIS_BOTTOM)->set_enabled(true);


	/* set some axes to linear and others to logarithmic scale */
//	m_plot.scale(PlotMM::AXIS_TOP)->set_range(10,1000,true);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_range(y_min,y_max,false);
//	m_plot.scale(PlotMM::AXIS_RIGHT)->set_range(10,1000,true);
	m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x_min,x_max);
	


	/* allow for autoscaling on all axes */
//	m_plot.scale(PlotMM::AXIS_TOP)->set_autoscale(true);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(true);
//	m_plot.scale(PlotMM::AXIS_RIGHT)->set_autoscale(true);
	m_plot.scale(PlotMM::AXIS_BOTTOM)->set_autoscale(false);

	set_plot_title(""); 

//	m_plot.label(PlotMM::AXIS_TOP)->set_text("top axis");
//	m_plot.label(PlotMM::AXIS_LEFT)->set_text("left axis");
//	m_plot.label(PlotMM::AXIS_RIGHT)->set_text("right axis");
	m_plot.label(PlotMM::AXIS_BOTTOM)->set_text( ("время") );

	plot_press = m_plot.signal_plot_mouse_press().connect(sigc::mem_fun(*this,&Oscillograph::on_plot_mouse_press));
	plot_release = m_plot.signal_plot_mouse_release().connect(sigc::mem_fun(*this,&Oscillograph::on_plot_mouse_release));
	m_plot.signal_plot_mouse_move().connect(sigc::mem_fun(*this,&Oscillograph::on_plot_mouse_move));

	set_time_interval(t_interval_sec);
		Gtk::Menu::MenuList& menulist = m_Menu_Popup.items();

		mi_stop = manage( new Gtk::MenuItem(("Остановить"),false) );
		mi_stop->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_stop));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_stop) );
		mi_stop->show();

		mi_start = manage( new Gtk::MenuItem(("Запустить"),false) );
		mi_start->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_start));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_start) );
		mi_start->hide();

		mi_unzoom = manage( new Gtk::MenuItem(("Востановить размер"),false) );
		mi_unzoom->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_unzoom));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_unzoom) );
		mi_unzoom->show();

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		mi_runner = manage( new Gtk::CheckMenuItem(("Бегунок"),false) );
		mi_runner->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_runner));
		menulist.push_back( Gtk::Menu_Helpers::CheckMenuElem(*mi_runner) );
		mi_runner->show();
		mi_runner->set_active(true);

		mi_x_axis = manage( new Gtk::CheckMenuItem(("Ось нуля"),false) );
		mi_x_axis->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_x_axis));
		menulist.push_back( Gtk::Menu_Helpers::CheckMenuElem(*mi_x_axis) );
		mi_x_axis->show();
		mi_x_axis->set_active(true);

//		mi_backtrace = manage( new Gtk::CheckMenuItem(("След"),false) );
//		mi_backtrace->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_backtrace));
//		menulist.push_back( Gtk::Menu_Helpers::CheckMenuElem(*mi_backtrace) );
//		mi_backtrace->show();
//		mi_backtrace->set_active(true);

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		mi_t_interval = manage( new Gtk::MenuItem(("Интервал..."),false) );
		mi_t_interval->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_set_interval));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_t_interval) );
		mi_t_interval->show();

		mi_t_step = manage( new Gtk::MenuItem(("Шаг времени..."),false) );
		mi_t_step->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_set_t_step));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_t_step) );
		mi_t_step->show();

//		Убрал меню настройки шага (он теперь прото расчитывается автоматически внутри tick()
//		mi_x_step = manage( new Gtk::MenuItem(("Шаг сдвига оси времени..."),false) );
//		mi_x_step->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_popup_set_x_step));
//		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_x_step) );
//		mi_x_step->show();

	m_Menu_Popup.accelerate(m_plot);

	x_axis.set_enabled( mi_x_axis->get_active() );
	set_enable_timer( true );
	
	time_t tm=time(0);
	struct tm *tms = localtime(&tm);
	tm_hour = tms->tm_hour;
	tm_min = tms->tm_min;
	tm_sec = tms->tm_sec;

//	btn_tmr.set_active(true);
}
//--------------------------------------------------------------------------
Oscillograph::Oscillograph( int numch, int up_msec , const std::string nm, const std::string sf):
	Gtk::Frame(),
	clst(numch),
	m_box0(false,5),
	m_tbl(1,2),
	plot_container(false,5),
	m_sb(),
	m_plot(),
	m_Menu_Popup()
{
	init( numch, up_msec, nm, sf );
}
//--------------------------------------------------------------------------
Oscillograph::Oscillograph( bool cycle_on_off, bool write_on_off, int numch, int up_msec , const std::string nm, const std::string sf):
	Gtk::Frame(),
	clst(numch),
	m_box0(false,5),
	m_tbl(1,2),
	plot_container(false,5),
	m_sb(),
	m_plot(),
	m_Menu_Popup()
{
	init( numch, up_msec, nm, sf );
	set_write_all_channels( write_on_off );
}
//--------------------------------------------------------------------------
Oscillograph::~Oscillograph()
{
	oscilnum--;

	unsigned int collection_length = clst.size();
	for(unsigned int i=0; i<collection_length; i++)
	{
		remove_channel(1);
	}
	m_Menu_Popup.items().clear();
}
//--------------------------------------------------------------------------
void Oscillograph::set_y_range(double min,double max)
{
	y_min = min;
	y_max = max;
	m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
	rn.set_length(y_min,y_max);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_range(y_min,y_max);
}
//--------------------------------------------------------------------------
void Oscillograph::set_plot_size( int width, int height )
{
	m_plot.set_size_request(width,height);
}
//--------------------------------------------------------------------------
void Oscillograph::set_plot_title( const std::string title )
{
	if( !title.empty() )
	{
		m_plot.title()->set_text(title);
		m_plot.title()->set_enabled(true);
	}
	else
		m_plot.title()->set_enabled(false);
}
//--------------------------------------------------------------------------
void Oscillograph::set_axis_label( PlotMM::PlotAxisID id, const Glib::ustring lbl )
{
	m_plot.label(id)->set_text(lbl);
}
//--------------------------------------------------------------------------
void Oscillograph::on_plot_mouse_press(int x,int y, GdkEventButton *ev)
{
   	print_coords(mx_=x,my_=y);

	if ((ev->button==1))  // zoom
	{
		mouse_is_pressed=true;
		/*
		if( enabled_timer() )
			return;
		m_plot.scale(PlotMM::AXIS_BOTTOM)->set_autoscale(false);
		m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
		zoomRect_.set_rect(x,y,0,0);
		m_plot.set_selection(zoomRect_);
		m_plot.enable_selection();
		*/
	} 
	else if ((ev->button==3))  // unzoom
		m_Menu_Popup.popup(ev->button, ev->time );
}
//--------------------------------------------------------------------------
void Oscillograph::on_plot_mouse_release(int x,int y, GdkEventButton *ev)
{
   	print_coords(mx_=x,my_=y);

	if( enabled_timer() )
		return;

	if( ev->button==1 ) // zoom
	{
		mouse_is_pressed=false;
		/*
		double x0,y0,x1,y1;
		int ix0,iy0,ix1,iy1;
		zoomRect_= m_plot.get_selection();
		ix0= zoomRect_.get_x_min();
		ix1= zoomRect_.get_x_max();
		iy0= zoomRect_.get_y_min();
		iy1= zoomRect_.get_y_max();
		x0= m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(ix0);
		x1= m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(ix1);
		y0= m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(iy1);
		y1= m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(iy0);
		m_plot.disable_selection();
		if (zoomRect_.get_width()==0 && zoomRect_.get_height()==0)
			return;

		if( !is_zooming )
			mi_unzoom->property_visible() = true;
		is_zooming = true;
		m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x0,x1);
		m_plot.scale(PlotMM::AXIS_LEFT)->set_range(y0,y1);
		m_plot.replot();
		*/
	}
}
//--------------------------------------------------------------------------
//данный обработчик подключается при нажатии кнопки "Названия каналов"
void Oscillograph::on_plot_mouse_tooltip_move(int x,int y, GdkEventMotion *ev)
{
	//формируем массив дистанций до кривых
	int* distances = new int[clst.size()];
	int i=0;
	for(ChannelList::iterator iter=clst.begin(); iter!=clst.end(); iter++, i++)
	{
		/*
			Суть алгоритма в том, что мы получаем из канала численное значение
			y-переменной, соответствующей передаваемой t на прямой.

			Потом это значение переводится в у-координату в пикселях на экране,
			которая и сравнивается с положением курсора.
		*/
		double etalon_y =iter->c->into_the_curve( m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(x));
		int scale_y=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().transform(etalon_y);
		int difference=abs(scale_y-y);
		distances[i]=difference;
	}
	int min_distance=distances[0];
	int min_distance_position=0;
	for(unsigned int j=1; j<clst.size(); j++)
	{
		if (min_distance>distances[j] && distances[j]>0)
		{
			min_distance=distances[j];
			min_distance_position=j;
		}
	}
	if (min_distance>0 && min_distance<y_tolerance)
		m_plot.set_canvas_tootip_text(clst.at(min_distance_position).c->get_title());
	else
		m_plot.set_canvas_tootip_text("\0");
	delete[] distances;
}
//--------------------------------------------------------------------------
void Oscillograph::on_plot_mouse_move(int x,int y, GdkEventMotion *ev)
{
	if( enabled_timer() )
	{
		print_coords(mx_=x,my_=y);
		return;
	}
	else
	{
		//здесь будет передвижение графика
		if (mouse_is_pressed)
		{
			int dx=mx_ - x;
			int dy=my_ - y;

			double x0, x1, y0, y1;
			x0=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(dx);
			x1=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(m_plot.get_canvas_width()+dx-1);
			y1=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(dy);
			y0=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(m_plot.get_canvas_height()+dy-1);

			m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x0,x1);
			m_plot.scale(PlotMM::AXIS_LEFT)->set_range(y0,y1);
			m_plot.replot();
		}
		/*zoomRect_.set_destination(x,y);
		m_plot.set_selection(zoomRect_);*/
		print_coords(mx_=x,my_=y);
	}

}
//--------------------------------------------------------------------------
void Oscillograph::print_coords( int x, int y )
{
	char tmp[100];
	snprintf(tmp,sizeof(tmp),"(%d,%d); t: %0.3f val: %0.4f",x,y,
			m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(x),
			m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(y) );
//	m_sb.pop();
	
	if (x>-1000&&y>-1000)
//		m_sb.push( (tmp) );
		m_sb_label->set_text((tmp));
}
//--------------------------------------------------------------------------
void Oscillograph::plot( int cnum, double x, double y )
{
	if( cnum < 1 || cnum > (int)clst.size() )
		return;

	clst[cnum-1].c->draw(x,y);
}
//--------------------------------------------------------------------------
bool Oscillograph::onUpdateTime()
{
	tick(t);
	t += t_step;
	return true;
}
//--------------------------------------------------------------------------
void Oscillograph::run()
{
	self_timer = true;
	set_self_timer(true);
}
//--------------------------------------------------------------------------
void Oscillograph::stop()
{
	self_timer = false;
	set_self_timer(false);
}
//--------------------------------------------------------------------------
bool Oscillograph::tick( const double _t )
{
	set_self_timer( false );
	t = _t;
	rn.draw(t);

	bool act = ( !self_timer || btn_tmr.get_active() ) && show_oscil;

	if( t >= x_max && act )
	{
		// насколько сдвигать экран...
		x_step = fabs( x_max - x_min )/4.0;
		x_min += x_step;
		x_max += x_step;
		m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x_min,x_max);
		x_axis.set_length(x_min,x_max);
	}

	if( x_axis.enabled() )
		x_axis.replot();
	
	double	min=0, max=0;
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		// предыдущее значение сохраняется
		if( (*it).c->get_backtrace() )
			(*it).c->draw( t, (*it).c->get_last_y() );

		(*it).c->replot();

		if( !(*it).c->enabled() )
			continue;

		double cmin = (*it).c->get_curve()->min_y_value();
		double cmax = (*it).c->get_curve()->max_y_value();

		min = cmin<y_min? (min<cmin? min:cmin):(min<y_min? min:y_min);
		max = cmax>y_max? (max>cmax? max:cmax):(max>y_max? max:y_max);
	}

	if( act )
	{
		if( rn.enabled() )
		{
//			double min = m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d1()!=-1? m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d1():y_min;
//			double max = m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d2()!=1? m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d2():y_max;
//			rn.set_length(	m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d1(),
//					m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().d2());
			rn.set_length(min,max);
			rn.replot();
		}

		m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(true);
		m_plot.replot();
/*
		stringstream s;
		s<<"секундомер: "<<fixed<<setprecision(2)<<t<<" сек; шаг "<<fixed<<setprecision(3)<<t_step<<" сек";
		btn_tmr_label->set_text((s.str()));
*/
		if( btn_tmr_label->is_realized() )
		{
			char tmp[150];
			snprintf(tmp,sizeof(tmp),"секундомер: %0.2f сек; шаг %0.3f сек;", t, t_step );
			Glib::ustring s(tmp);
			btn_tmr_label->set_text(s);
//			btn_tmr_label->queue_resize_no_redraw();
//			btn_tmr_label->queue_draw();
		}
	}
	
	// почему-то если вызывать tick самостоятельно
	// не прорисовываются графики
	queue_draw();
	while( Gtk::Main::events_pending() )
		Gtk::Main::iteration();

	set_self_timer( true );
	return true;
}
//--------------------------------------------------------------------------
void Oscillograph::set_time_interval( double new_t_interval_sec )
{
	if( new_t_interval_sec == t_interval_sec )
		return;

	//моё неуважение автору того, что тут было раньше

	if( new_t_interval_sec <=0 )
	{
		std::cerr<<"Wrong input parameters"<<std::endl;
		return;
	}

	double xp;
	if(t_interval_sec <= 0)
		xp = 0.2; //default
	else
		xp = x_step / t_interval_sec;

	t_interval_sec = new_t_interval_sec;
	x_step = xp * t_interval_sec;

	if(t_step>x_step)
	{
		x_step = t_step;
	}

	x_max = x_min + t_interval_sec;
	m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x_min,x_max);
	x_axis.set_length(x_min,x_max);
	int pcount = lroundf( t_interval_sec / t_step );
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		(*it).c->set_size(pcount);
	m_plot.replot();			
}
//--------------------------------------------------------------------------
void Oscillograph::set_timer_step( int msec )
{
	if( msec == tmr_msec )
		return;

//	int tmr_old = tmr_msec;
	tmr_msec = msec;

	if( tmr_enabled )
		btn_tmr.set_active(true);

	set_self_timer(tmr_enabled);
	
	t_step = tmr_msec / 1000.0;

	int pcount = lroundf( t_interval_sec / t_step );
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		(*it).c->set_size( pcount );	// устанавливаем сколько точек должно хранится

//	m_timestep_signal.emit( tmr_old, msec, this );
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_set_interval()
{
	InputValueDialog d("Ввод интервала просмотра");
	d.set_increments(1.0,5.0,1);
	set_time_interval( d.run(t_interval_sec,max(t_step,1.0),MaxValue,"сек") );
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_set_t_step()
{
	InputValueDialog d("Ввод шага времени");
	d.set_increments(1.0,10.0,1);
	double val = d.run( (t_step*1000.0), MinTimer_msec, min(t_interval_sec*1000, MaxTimer_msec),"мсек");

	int tmr_old = tmr_msec;
	set_timer_step( lroundf(val) );
	m_timestep_signal.emit( tmr_old, lroundf(val), this );
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_set_x_step()
{
	InputValueDialog d("Ввод сдвига по оси");
	d.set_increments( 1, 5, 0 );

	double xp = x_step / t_interval_sec * 100.0;
	xp = d.run(xp,(int)(t_step/t_interval_sec*100-0.1)+1,100,"%");
	x_step = ( xp / 100.0 ) * t_interval_sec;
	if( t_step>x_step ) x_step = t_step;
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_unzoom()
{
	unzoom();
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_stop()
{
	set_enable_timer(false);
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_start()
{
	set_enable_timer(true);
}
//--------------------------------------------------------------------------
void Oscillograph::unzoom()
{
	m_plot.disable_selection();
	m_plot.scale(PlotMM::AXIS_BOTTOM)->set_autoscale(false);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(true);
	// т.к. в set_time_interval
	// стоит проверка на изменение
	// поэтому имитируем изменение
	t_interval_sec -=1;
	set_time_interval( t_interval_sec+1 );
	is_zooming = false;
	mi_unzoom->property_visible() = false;
	m_plot.replot();
}
//--------------------------------------------------------------------------
void Oscillograph::on_but_timer_toggled()
{
	set_enable_timer( btn_tmr.get_active() );
	//rn.set_enabled(btn_tmr.get_active());
	if( t >= x_max && btn_tmr.get_active())
	{
		x_min = t - (x_max-x_min);
		x_max = t;
		m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x_min,x_max);
		x_axis.set_length(x_min,x_max);
//		m_plot.replot();
	}

	/*for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		(*it).c->get_curve()->set_enabled(
			(btn_tmr.get_active()) && ((*it).c->get_button()->get_active())
			);
	}*/
}
//--------------------------------------------------------------------------
void Oscillograph::channel_toggled(int numOwnNotebook,int numCurrentNotebook)
{
	if(show_oscil==(numOwnNotebook==numCurrentNotebook))
		return;
	show_oscil=(numOwnNotebook==numCurrentNotebook);
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		(*it).c->get_curve()->set_enabled((numOwnNotebook==numCurrentNotebook) && (btn_tmr.get_active()) && ((*it).c->get_button()->get_active()));
	}
	rn.set_enabled(btn_tmr.get_active() && show_oscil);
	if( t >= x_max && btn_tmr.get_active() && show_oscil)
	{
		x_min = t - (x_max-x_min);
		x_max = t;
		m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(x_min,x_max);
		x_axis.set_length(x_min,x_max);
//		m_plot.replot();
	}
}
//--------------------------------------------------------------------------
void Oscillograph::set_self_timer( bool st )
{
	if( !self_timer )
		return;
		
	if( tmr )
		tmr.disconnect();

	if( st )
	{
		tmr = Glib::signal_timeout().connect(
				sigc::mem_fun(*this, &Oscillograph::onUpdateTime), tmr_msec);
		m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(true);
	}
	else
	{
		m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
	}
}
//--------------------------------------------------------------------------
Oscillograph::signal_ChannelRemove Oscillograph::signal_channel_remove()
{
	return s_ch_remove;
}
//--------------------------------------------------------------------------
void Oscillograph::set_enable_timer( bool st )
{
	set_self_timer( st );

	if( tmr_enabled == st )
		return;

	tmr_enabled = st;

	if( tmr_enabled )
	{
		unzoom();
		mi_unzoom->set_sensitive(false);
		mi_stop->show();
		mi_start->hide();
	}
	else
	{
		mi_unzoom->set_sensitive(true);
		mi_stop->hide();
		mi_start->show();
	}
	btn_tmr.set_active(st);
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_runner()
{
	rn.set_enabled( mi_runner->get_active() );
}
//--------------------------------------------------------------------------
Glib::RefPtr<Channel> Oscillograph::add_channel(const Glib::ustring lbl, Gdk::Color clr , std::string filename,std::string tmpdir, int size, int filesz)
{
	int ind = clst.size();
	clst.resize( ind+1 );

	if( clst[ind].c!= 0 )
		cerr << "(add_channel): c=" << ind+1 << " != NULL " << endl;
	
	std::ostringstream s;
	if( filename.empty() )
		s << myname << "_channel" << setw(2) << setfill('0') << ind;
	else
		s << myname << "_" << filename;

	ChannelInfo cinfo;
	//Channel* c = new Channel(sessionname, s.str(), tmpdir);
	//cinfo.c=new Channel(sessionname, s.str(), tmpdir);

	//Glib::RefPtr<Channel> c_tmp(new Channel(sessionname, s.str(), tmpdir));
	//cinfo.c=Channel::create(s.str(), tmpdir);
	Glib::RefPtr<Channel> temp(new Channel(s.str(), tmpdir, size, filesz));
	cinfo.c=temp;
	init_channel( cinfo );

	cinfo.c->set_title(lbl);
	cinfo.c->set_color(clr);
	
	int pcount = lround( t_interval_sec / t_step );
	cinfo.c->set_size(pcount);

	Gtk::Menu::MenuList& menulist = cinfo.c->get_menu();
	{
		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		Glib::ustring toggle_writing_text;
		if (cinfo.c->isWriting()) {
			toggle_writing_text = STOP_WRITING_CHANNEL_TEXT;
		} else {
			toggle_writing_text = START_WRITING_CHANNEL_TEXT;
		}
		mi_toggle_writing = manage( new Gtk::MenuItem((toggle_writing_text),false) );
		cinfo.toggle_writing_connection = mi_toggle_writing->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::on_menu_toggle_writing), cinfo.c));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_toggle_writing) );
		mi_toggle_writing->show();

		Glib::ustring toggle_writing_all_text;
		if(!write_all_channels)
			toggle_writing_all_text = START_WRITING_ALL_CHANNELS_TEXT;
		else
			toggle_writing_all_text = STOP_WRITING_ALL_CHANNELS_TEXT;
		mi_toggle_writing_all = manage( new Gtk::MenuItem((toggle_writing_all_text), false) );
		mi_toggle_writing_all->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::on_menu_toggle_writing_all));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_toggle_writing_all) );
		mi_toggle_writing_all->show();

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		mi_save = manage( new Gtk::MenuItem((SAVE_TO_FILE_TEXT),false) );
		cinfo.save_data_connection = mi_save->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::on_menu_save_file), cinfo.c));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_save) );
		mi_save->show();
		
		mi_save_all = manage( new Gtk::MenuItem((SAVE_ALL_TO_FILES_TEXT),false) );
		mi_save_all->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::save_all_channels));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_save_all) );
		mi_save_all->show();
	
		mi_save_all_in_one = manage( new Gtk::MenuItem((SAVE_ALL_TO_ONE_FILE_TEXT),false) );
		mi_save_all_in_one->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::save_all_channels_in_one));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_save_all_in_one) );
		mi_save_all_in_one->show();

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );
	
		mi_on_all = manage( new Gtk::MenuItem((TURN_ON_ALL_CHANNELS_TEXT),false) );
		mi_on_all->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::off_on_all_channels),true));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_on_all) );
		mi_on_all->show();

		mi_off_all = manage( new Gtk::MenuItem((TURN_OFF_ALL_CHANNELS_TEXT),false) );
		mi_off_all->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::off_on_all_channels),false));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_off_all) );
		mi_off_all->show();

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		mi_remove = manage( new Gtk::MenuItem((DELETE_CHANNEL_TEXT), false) );
		cinfo.remove_channel_connection = mi_remove->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::on_menu_remove_channel), cinfo.c));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_remove) );
		mi_remove->show();

		menulist.push_back( Gtk::Menu_Helpers::SeparatorElem() );

		if( cinfo.c->is_rewriting() )
			mi_toggle_cycle = manage( new Gtk::MenuItem((TURN_OFF_CYCLE_TEXT),false) );
		else
			mi_toggle_cycle = manage( new Gtk::MenuItem((TURN_ON_CYCLE_TEXT),false) );
		mi_toggle_cycle->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::off_on_cycle), cinfo.c));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_toggle_cycle) );
		mi_toggle_cycle->show();

		if( cycle_save_all_channels )
			mi_toggle_cycle_all = manage( new Gtk::MenuItem((TURN_OFF_ALL_CYCLE_TEXT),false) );
		else
			mi_toggle_cycle_all = manage( new Gtk::MenuItem((TURN_ON_ALL_CYCLE_TEXT),false) );
		mi_toggle_cycle_all->signal_activate().connect(sigc::mem_fun(*this, &Oscillograph::off_on_cycle_all));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_toggle_cycle_all) );
		mi_toggle_cycle_all->show();

		mi_set_file_history_max_point = manage( new Gtk::MenuItem((SET_TEMP_FILE_TEXT), false) );
		mi_set_file_history_max_point->signal_activate().connect(bind(sigc::mem_fun(*this, &Oscillograph::on_menu_set_file_history_max_point), cinfo.c));
		menulist.push_back( Gtk::Menu_Helpers::MenuElem(*mi_set_file_history_max_point) );
		mi_set_file_history_max_point->show();
	}

	cinfo.c->set_rewrite(cycle_save_all_channels);

	if (write_all_channels)
		cinfo.c->startWriting();

	//std::cout<<"pushing back"<<std::endl;
	clst[ind] = cinfo;
	//std::cout<<" add_channel() to be ended"<<std::endl;
	return cinfo.c;
}
//--------------------------------------------------------------------------
Glib::RefPtr<Channel> Oscillograph::add_channel(const Glib::ustring lbl, Gdk::Color clr, int size, int filesz, string filename, string tmpdir)
{
	return add_channel(lbl, clr, filename, tmpdir, size, filesz);
}
//--------------------------------------------------------------------------
void Oscillograph::remove_channel(Glib::RefPtr<Channel>& c)
{
	this->remove_channel(get_number(c));
}
//--------------------------------------------------------------------------

void Oscillograph::remove_channel(int cnum)
{
	if (cnum<1)
		return;
	ChannelInfo cinfo=get_channel_info(cnum);

	m_tbl.remove( *(cinfo.c->get_y_frm()) );
	m_tbl.remove( *(cinfo.c->get_button()) );

	cinfo.save_data_connection.disconnect();
	cinfo.remove_channel_connection.disconnect();
	cinfo.toggle_writing_connection.disconnect();
	delete cinfo.c->get_button();

	clst.erase(std::remove( clst.begin(), clst.end(), cinfo ), clst.end());

	int rows = m_tbl.property_n_rows();
	int cols = m_tbl.property_n_rows();
	m_tbl.resize(rows-1, cols);

}
//--------------------------------------------------------------------------
void Oscillograph::save_channel_with_save_dialog(Glib::RefPtr<Channel>& c)
{
	c->save_channel_with_save_dialog(name, myname, savefolder);
}
//--------------------------------------------------------------------------
void Oscillograph::on_menu_toggle_writing(Glib::RefPtr<Channel>& c)
{
	Gtk::Menu::MenuList menulist = c->get_menu();
	bool global_write_settings = true;//checking if all channels are having same option
	if (c->isWriting())
	{
		c->stopWriting();
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			if((*it).c->isWriting())
			{
				global_write_settings = false;
				break;
			}
		}
		if(global_write_settings && write_all_channels)//if we've stopped all channels
			on_menu_toggle_writing_all();
		//set_cycle(c, cycle_save_all_channels);
		if(cycle_save_all_channels)
			menulist[15].set_label(TURN_OFF_ALL_CYCLE_TEXT);
		else
			menulist[15].set_label(TURN_ON_ALL_CYCLE_TEXT);
	}
	else
	{
		c->startWriting();
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			if(!(*it).c->isWriting())
			{
				global_write_settings = false;
				break;
			}
		}
		if(global_write_settings && !write_all_channels)//if we've started all channels
			on_menu_toggle_writing_all();
	}
}
//--------------------------------------------------------------------------
void Oscillograph::on_menu_toggle_writing_all()
{
	if(!write_all_channels)
	{
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			Gtk::Menu::MenuList& menulist = (*it).c->get_menu();
			menulist[3].set_label(STOP_WRITING_ALL_CHANNELS_TEXT);
			if((*it).c->isWriting())
				continue;
			(*it).c->startWriting();
		}
		write_all_channels = true;
	}
	else
	{
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			Gtk::Menu::MenuList& menulist = (*it).c->get_menu();
			menulist[3].set_label(START_WRITING_ALL_CHANNELS_TEXT);
			(*it).c->stopWriting();
			//set_cycle((*it).c, cycle_save_all_channels);
			if(cycle_save_all_channels)
				menulist[15].set_label(TURN_OFF_ALL_CYCLE_TEXT);
			else
				menulist[15].set_label(TURN_ON_ALL_CYCLE_TEXT);
			}
		write_all_channels = false;
	}
}
//--------------------------------------------------------------------------
void Oscillograph::on_menu_save_file(Glib::RefPtr<Channel>& c)
{
	save_channel_with_save_dialog(c);
}
//--------------------------------------------------------------------------
void Oscillograph::on_menu_remove_channel(Glib::RefPtr<Channel>& c)
{
	s_ch_remove.emit(get_number(c),this);
}
//--------------------------------------------------------------------------
void Oscillograph::on_menu_set_file_history_max_point(Glib::RefPtr<Channel>& c)
{
	double kB = (double)c->get_item_size()/1024;
	double minutes = get_timer_step()/60;
	ConnectedValueDialog d("Размер временного файла", kB, minutes);
	d.set_increments(1,100,2);
	d.set_labels("Записей", "кБ", "Минут");
	c->set_file_history_max_point(d.run(c->get_file_history_max_point(),1,CHANNEL_TEMP_FILE_MAX_SIZE));
}
//--------------------------------------------------------------------------
void Oscillograph::save_in_file( Glib::RefPtr<Channel>& c, string filename)
{
	c->save_in_file(filename, 0, 0);
}
//--------------------------------------------------------------------------
void Oscillograph::save_all_channels( )
{
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		if(!(*it).c->enabled())
			continue;
		string filename = (*it).c->get_button()->get_label();
		if( filename.empty() )
			continue;
		else
		{
			if(!(*it).c->get_folder().empty())
				filename = (*it).c->get_folder() + "/" + (*it).c->dateToString(time(0),"") + "_" + (*it).c->timeToString(time(0),"") + "_" + ((name.empty()? myname:name).c_str()) + "_" + filename + ".csv";
			else
				filename = (*it).c->dateToString(time(0),"")+ "_" + (*it).c->timeToString(time(0),"") + "_" + (name.empty()? myname:name) + "_" + filename + ".csv";
		}
		save_in_file( (*it).c, filename);
	}
}
//--------------------------------------------------------------------------
void Oscillograph::write_to_stringstreams(stringstream& stime, stringstream& sy, double x, double y, int hour, int min, int sec)
{
	stime<<"\n"<<hour<<":"<<setw(2)<<setfill('0')<<min<<":"<<setw(2)<<setfill('0')<<sec;
	stime<<",\""<<(long)x<<","<<setw(5)<<setfill('0')<<(long)((x-(long)x)*100000);
	stime<<"\"";
	sy<<",\""<< (y>=0? "":"-") <<abs((long)y)<<","<<setw(5)<<setfill('0')<<abs((long)((y-(long)y)*100000));
	sy<<"\"";
}
//--------------------------------------------------------------------------
Oscillograph::Line Oscillograph::create_file_line(double x, double y, int hour, int min, int sec, unsigned int cnum)
{
	Line line(clst.size()+1);
	stringstream stime, sy;
	write_to_stringstreams(stime,sy,x,y,hour,min,sec);

	line[0] = stime.str();
	line[cnum] = sy.str();

	return line;
}
//--------------------------------------------------------------------------
void Oscillograph::save_all_channels_in_one( )
{
	if(clst.empty())
		return;

	ChannelList::iterator firstCH = clst.begin();
	string filename;
	if(!(*firstCH).c->get_folder().empty())
		filename = (*firstCH).c->get_folder() + "/" + (*firstCH).c->dateToString(time(0),"") + "_" + (*firstCH).c->timeToString(time(0),"") + "_" + (name.empty()? myname:name) + ".csv";
	else
		filename = (*firstCH).c->dateToString(time(0),"") + "_" + (*firstCH).c->timeToString(time(0),"") + "_" + (name.empty()? myname:name) + ".csv";
	
	cout<<"файл: "<<filename<<endl;

	cout<<"попытка записи в файл: "<<filename<<endl;
	fstream fout( filename.c_str(), ios::out | ios::trunc );
	if(!fout)
	{
		cerr << "НЕ СМОГ ОТКРЫТЬ ФАЙЛ НА ЗАПИСЬ " << filename << endl;
		return;
	}

	std::list<PlotData> data;
	std::list<PlotHistory> history;

	unsigned int cnum = 0;
	Matrix matrix;

	History tmpHistory;
	stringstream s;
	s<<"Время,\"Отсчёт,[сек]\"";
	#warning problems with system symbols in label name when trying to open it in oocalc or excel
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); it++ )
	{
		cnum++;
		if(!(*it).c->enabled())
			continue;

		s<<",\""<<(*it).c->get_button()->get_label()<<"\"";

		(*it).c->goHistoryHead();
		while( (*it).c->readNextHistory(tmpHistory) )
		{
			double x	= tmpHistory.x;
			double y	= tmpHistory.y;
			int hour	= tmpHistory.time.hour;
			int min		= tmpHistory.time.min;
			int sec		= tmpHistory.time.sec;

			if(matrix.find(x)!=matrix.end())
			{
				if(matrix[x].size() > cnum)
				{
					stringstream stime, sy;
					write_to_stringstreams(stime,sy,x,y,hour,min,sec);
					matrix[x][0] = stime.str();
					matrix[x][cnum] = sy.str();
				}
				else
					cout << myname << ": не существует канала с номером" << cnum << endl;
			}
			else
				matrix[x] = create_file_line(x,y,hour,min,sec,cnum);
		}

		double x_m = 0;
		double x_mx = (*it).c->get_last_x();

		for(std::list<History>::iterator hIt = (*it).c->get_history().begin(); hIt!=(*it).c->get_history().end(); ++hIt )
		{
			double x	= (*hIt).x;
			if( x < x_m && x > x_mx )
				continue;
			double y	= (*hIt).y;
			int hour	= (*hIt).time.hour;
			int min		= (*hIt).time.min;
			int sec		= (*hIt).time.sec;

			if(matrix.find(x)!=matrix.end())
			{
				if(matrix[x].size() > cnum)
				{
					stringstream stime, sy;
					write_to_stringstreams(stime,sy,x,y,hour,min,sec);
					matrix[x][0] = stime.str();
					matrix[x][cnum] = sy.str();
				}
				else
					cout << myname << ": не существует канала с номером " << cnum << endl;
			}
			else
				matrix[x] = create_file_line(x,y,hour,min,sec,cnum);
		}

		// save current data
		for( std::list<PlotMM::DoublePoint>::iterator dIt = (*it).c->get_data().begin(); dIt!=(*it).c->get_data().end(); ++dIt )
		{
			double x=dIt->get_x();
			if( x < x_m && x > x_mx )
				continue;
			double y=dIt->get_y();
			int hour;
			int min;
			int sec;
			hour	= (*it).c->get_dataT()[x].hour;
			min	= (*it).c->get_dataT()[x].min;
			sec	= (*it).c->get_dataT()[x].sec;

			if(matrix.find(x)!=matrix.end())
			{
				if(matrix[x].size() > cnum)
				{
					stringstream stime, sy;
					write_to_stringstreams(stime,sy,x,y,hour,min,sec);
					matrix[x][0] = stime.str();
					matrix[x][cnum] = sy.str();
				}
				else
					cout << myname << ": не существует канала с номером " << cnum << endl;
			}
			else
				matrix[x] = create_file_line(x,y,hour,min,sec,cnum);
		}
	}

	Matrix::iterator mIt;
	for( mIt=matrix.begin(); mIt!=matrix.end(); mIt++ )
	{
		s<<(mIt->second)[0];
		for(unsigned int num = 1; num <= clst.size(); num++)
		{
			if(!clst[num-1].c->enabled())
				continue;
			if((mIt->second)[num].empty())
				s<<",";
			else
				s<<(mIt->second)[num];
		}
	}


	fout<<Glib::convert((s.str()),"UNICODE","UTF8");//conversion from utf8 used here to utf16 (only encoding supported in ms office)
	fout.close();
}
//--------------------------------------------------------------------------
void Oscillograph::timeToInt(long time,int &hour, int &min, int &sec)
{
	hour = tm_hour + time / 3600;
	while(hour>=24)
		hour = hour - 24;
	min = tm_min + (time % 3600) / 60;
	if(min>=60)
	{
		min = min - 60;
		hour++;
		if(hour>=24)
			hour = hour - 24;
	}
	sec = tm_sec + (time % 3600) % 60;
	if(sec>=60)
	{
		sec = sec - 60;
		min++;
		if(min>=60)
		{
			min = min - 60;
			hour++;
			if(hour>=24)
				hour = hour - 24;
		}
	}
}
//--------------------------------------------------------------------------
void Oscillograph::set_cycle(Glib::RefPtr<Channel>& c, bool state)
{
	c->set_rewrite(state);
	bool global_rewrite_settings = true;
	if(cycle_save_all_channels == state) return;//we don't need to toggle global menu item
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		if ((*it).c->isWriting())
			continue;
		if((*it).c->is_rewriting() != state)
		{
			global_rewrite_settings = false;
			break;
		}
	}
	if(global_rewrite_settings)
		off_on_cycle_all();
}
//--------------------------------------------------------------------------
void Oscillograph::off_on_cycle(Glib::RefPtr<Channel>& c)
{
	set_cycle(c, !c->is_rewriting());
}
//--------------------------------------------------------------------------
void Oscillograph::set_cycle_rewrite_all(bool state)
{
	if(!state)
	{
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			if ((*it).c->isWriting())
				continue;
			(*it).c->set_rewrite(false);
			Gtk::Menu::MenuList& menulist = (*it).c->get_menu();
			menulist[15].set_label(TURN_ON_ALL_CYCLE_TEXT);
		}
	}
	else
	{
		for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		{
			if ((*it).c->isWriting())
				continue;
			(*it).c->set_rewrite(true);
			Gtk::Menu::MenuList& menulist = (*it).c->get_menu();
			menulist[15].set_label(TURN_OFF_ALL_CYCLE_TEXT);
		}
	}
}
//--------------------------------------------------------------------------
void Oscillograph::off_on_cycle_all()
{
	cycle_save_all_channels = !cycle_save_all_channels;
	set_cycle_rewrite_all(cycle_save_all_channels);
}
//--------------------------------------------------------------------------
void Oscillograph::off_on_all_channels( bool mark )
{
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		if((*it).c->get_button()->is_sensitive())
			(*it).c->get_button()->set_active(mark);
	}
}
//--------------------------------------------------------------------------
Oscillograph::ChannelInfo Oscillograph::get_channel_info( Glib::RefPtr<Channel>& c )
{
	int num = get_number(c);
	if( num < 0 )
		return ChannelInfo();
		
	return get_channel_info(num);
}
//--------------------------------------------------------------------------

Oscillograph::ChannelInfo Oscillograph::get_channel_info( int num )
{
	if( num < 1 || num > (int)clst.size() )
	{
		return ChannelInfo();
	}

	return clst[num-1];
}
//--------------------------------------------------------------------------
void Oscillograph::init_channel( ChannelInfo& cinfo )
{
	if( !cinfo.c )
	{
		cerr << "(init_channel): NULL pointer..." << endl;
		return;
	}

	m_plot.add_curve(cinfo.c->get_curve());

	int rows = m_tbl.property_n_rows();
	int cols = m_tbl.property_n_rows();

	//cinfo.f= manage( new Gtk::Frame() );
	cinfo.c->get_y_label()->modify_bg(Gtk::STATE_NORMAL,Gdk::Color("white"));
	cinfo.c->get_y_label()->modify_fg(Gtk::STATE_NORMAL,Gdk::Color("black"));
	cinfo.c->get_button()->signal_toggled().connect(sigc::mem_fun(*this,&Oscillograph::on_channel_btn_toggled));

	m_tbl.resize(rows+1, cols);
	m_tbl.attach( *(cinfo.c->get_y_frm()), 0,1,rows,rows+1,Gtk::FILL|Gtk::SHRINK,Gtk::FILL|Gtk::SHRINK);
	m_tbl.attach( *(cinfo.c->get_button()), 1,2,rows,rows+1,Gtk::FILL|Gtk::EXPAND,Gtk::FILL|Gtk::SHRINK);
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_backtrace()
{
	set_backtrace_enable( mi_backtrace->get_active() );
}
//--------------------------------------------------------------------------
void Oscillograph::set_backtrace_enable( bool bt )
{
	if( backtrace == bt )
		return;

	backtrace = bt;
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		(*it).c->set_backtrace(bt);
}
//--------------------------------------------------------------------------
Oscillograph::SetTimeStep_Signal Oscillograph::signal_set_timer_step()
{
	return m_timestep_signal;
}
// -------------------------------------------------------------------------
unsigned int Oscillograph::all_history_max_kbytes()
{
	unsigned int kbytes = 0;
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
		 kbytes += (*it).c->get_history_max_kbytes();

	return kbytes;
}
// -------------------------------------------------------------------------
int Oscillograph::get_number( Glib::RefPtr<Channel>& c )
{
	int i=1;
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it, i++ )
	{
		if( (*it).c == c )
			return i;
	}

	return -1;
}
//--------------------------------------------------------------------------
void Oscillograph::set_legend_width( int w )
{
//	scwin->set_size_request(w);
	b_frm->set_size_request(w);
}
//--------------------------------------------------------------------------
void Oscillograph::menu_runner_set_sensitive( bool state )
{
	mi_runner->set_sensitive(state);
}
//--------------------------------------------------------------------------
void Oscillograph::menu_backtrace_set_sensitive( bool state )
{
	mi_backtrace->set_sensitive(state);
}
//--------------------------------------------------------------------------
void Oscillograph::menu_t_interval_set_sensitive( bool state )
{
	mi_t_interval->set_sensitive(state);
}
//--------------------------------------------------------------------------
void Oscillograph::menu_t_step_set_sensitive( bool state )
{
	mi_t_step->set_sensitive(state);
}
//--------------------------------------------------------------------------
void Oscillograph::menu_x_step_set_sensitive( bool state )
{
	mi_x_step->set_sensitive(state);
}
//--------------------------------------------------------------------------
void Oscillograph::on_quest_button_toggled()
{
	if (quest_button->get_active())
	{
		mouse_tooltip_move = m_plot.signal_plot_mouse_move().connect(sigc::mem_fun(*this,&Oscillograph::on_plot_mouse_tooltip_move));
	}
	else
	{
		mouse_tooltip_move.disconnect();
		m_plot.set_canvas_tootip_text("\0");
	}
}
//--------------------------------------------------------------------------
void Oscillograph::on_channel_btn_toggled()
{
	// ищем максимальное значение и настраиваем scale...	
	double min = y_min;
	double max = y_max;
	double cmax= 0;
	double cmin= 0;
	
	for( ChannelList::iterator it=clst.begin(); it!=clst.end(); ++it )
	{
		if( !(*it).c->enabled() )
			continue;
		
		cmax = (*it).c->get_curve()->max_y_value();
		cmin = (*it).c->get_curve()->min_y_value();

		if( cmin < min )
			min = cmin;

		if( cmax > max )
			max = cmax;
	}
	
	m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
	m_plot.scale(PlotMM::AXIS_LEFT)->set_range(min,max);
}
//--------------------------------------------------------------------------
void Oscillograph::on_popup_x_axis()
{
	x_axis.set_enabled( mi_x_axis->get_active() );
}
//--------------------------------------------------------------------------
bool Oscillograph::on_plot_scroll_event(GdkEventScroll* event)
{
	if (!enabled_timer())
	{
		if (event->direction==GDK_SCROLL_UP)	//приближение к
		{
			int width=m_plot.get_canvas_width();
			int height=m_plot.get_canvas_height();
			double c_x0, c_x1, c_y0, c_y1, cursor_x, cursor_y;
			c_x0=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(0);
			c_x1=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(width);
			cursor_x=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(mx_);
			c_y1=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(0);
			c_y0=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(height);
			cursor_y=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(my_);
			//std::cout<<c_x0<<" "<<c_x1<<" "<<cursor_x<<"    "<<c_y0<<" "<<c_y1<<" "<<cursor_y<<std::endl;

			double c_length=c_x1-c_x0;
			double c_height=c_y1-c_y0;
			double n_length=c_length*approaching_coefficient;
			double n_height=c_height*approaching_coefficient;
			//std::cout<<"c_l: "<<c_length<<"    c_h: "<<c_height<<std::endl;
			//std::cout<<"n_l: "<<n_length<<"    n_h: "<<n_height<<std::endl;

			double n_x0, n_x1, n_y0, n_y1;
			/*
			* Идея алгоритма в том, что мы смотрим какую долю от размеров
			* прямоугольника составляет расстояние от курсора до границ этого
			* прямоугольника. При приближении лишь остается соблюсти пропорции.
			*/
			double percentage_x = (cursor_x - c_x0) / c_length;
			double percentage_y = (cursor_y - c_y0) / c_height;

			double left_distance, right_distance, up_distance, down_distance;
			left_distance = percentage_x * n_length;
			right_distance = (1 - percentage_x) * n_length;
			down_distance = percentage_y * n_height;
			up_distance = (1 - percentage_y) * n_height;

			n_x0 = cursor_x - left_distance;
			n_x1 = cursor_x + right_distance;
			n_y0 = cursor_y - down_distance;
			n_y1 = cursor_y + up_distance;

			//std::cout<<n_x0<<" "<<n_x1<<"    "<<n_y0<<" "<<n_y1<<"\n\n"<<std::endl;
			if (!is_zooming)
			{
				is_zooming = true;
				m_plot.scale(PlotMM::AXIS_BOTTOM)->set_autoscale(false);
				m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
				mi_unzoom->property_visible() = true;
			}
			m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(n_x0,n_x1);
			m_plot.scale(PlotMM::AXIS_LEFT)->set_range(n_y0,n_y1);
			m_plot.replot();
			
		}
		else if (event->direction==GDK_SCROLL_DOWN)	//удаление от
		{
			double c_x0, c_x1, c_y0, c_y1;
			c_x0=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(0);
			c_x1=m_plot.scale(PlotMM::AXIS_BOTTOM)->scale_map().inv_transform(m_plot.get_canvas_width());
			c_y1=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(0);
			c_y0=m_plot.scale(PlotMM::AXIS_LEFT)->scale_map().inv_transform(m_plot.get_canvas_height());
			//std::cout<<c_x0<<" "<<c_x1<<"   "<<c_y0<<" "<<c_y1<<std::endl;

			double c_length=c_x1-c_x0;
			double c_height=c_y1-c_y0;
			double c_center_x=(c_x0+c_x1)/2;
			double c_center_y=(c_y0+c_y1)/2;
			double n_length=c_length/approaching_coefficient;
			double n_height=c_height/approaching_coefficient;

			double n_x0,n_x1,n_y0,n_y1;
			n_x0=c_center_x - n_length/2;
			n_x1=c_center_x + n_length/2;
			n_y0=c_center_y - n_height/2;
			n_y1=c_center_y + n_height/2;

			//std::cout<<n_x0<<" "<<n_x1<<"   "<<n_y0<<" "<<n_y1<<"\n\n"<<std::endl;

			m_plot.scale(PlotMM::AXIS_BOTTOM)->set_range(n_x0, n_x1);
			m_plot.scale(PlotMM::AXIS_LEFT)->set_range(n_y0,n_y1);
			m_plot.replot();
			if (!is_zooming)
			{
				is_zooming = true;
				m_plot.scale(PlotMM::AXIS_BOTTOM)->set_autoscale(false);
				m_plot.scale(PlotMM::AXIS_LEFT)->set_autoscale(false);
				mi_unzoom->property_visible() = true;
			}
		}
	}
	return 1;
}
//--------------------------------------------------------------------------
void Oscillograph::set_enable_x_axis( bool state )
{
	mi_x_axis->set_active(state);
}
//--------------------------------------------------------------------------
void Oscillograph::set_enable_runner( bool state )
{
	mi_runner->set_active(state);
}
//--------------------------------------------------------------------------
bool Oscillograph::whether_color_is_taken(Gdk::Color* col)
{
	Gdk::Color* col_tmp;
	ushort reiteration;
	//При относительном совпадении предложенного цвета с одни из уже имеющихся хотя
	//бы по 2-м параметрам из трех, предложенный бракуется.
	for (ChannelList::iterator it=clst.begin(); it!=clst.end(); it++)
	{
		reiteration=0;
		col_tmp=it->c->get_color();
		if (abs( col_tmp->get_red() - col->get_red() ) < color_tolerance )
			reiteration++;
		if (abs( col_tmp->get_blue() - col->get_blue() ) < color_tolerance )
			reiteration++;
		if (abs( col_tmp->get_green() - col->get_green() ) < color_tolerance )
			reiteration++;

		if (reiteration>1)
			return true;
	}
	return false;
}
//--------------------------------------------------------------------------
void Oscillograph::set_write_all_channels(bool state)
{
	write_all_channels = state;
}
//--------------------------------------------------------------------------
void Oscillograph::disable_auxlabels( bool sw )
{
	if(sw)
	{
		quest_button->hide();
		btn_tmr_label->hide();
		m_sb_label->hide();
		btn_tmr.hide();
	}
}