//$Id: Channel.h,v 1.17 2009/12/16 07:13:17 alex Exp $
// -----------------------------------------------------------------------------
#ifndef _CHANNEL_H
#define _CHANNEL_H
// -----------------------------------------------------------------------------
#include <gtkmm.h>
#include <list>
#include <plotmm/paint.h>
#include <plotmm/curve.h>
#include "StorInterface.h"

#include <sstream>
#include <iostream>
#include <fstream>
//--------------------------------------------------------------------------
using namespace std;
// -----------------------------------------------------------------------------
static const int CHANNEL_TEMP_FILE_MAX_SIZE = 10000000;
static const std::string STOP_WRITING_CHANNEL_TEXT = "Остановить запись канала";
static const std::string START_WRITING_CHANNEL_TEXT = "Начать запись канала";
static const std::string TURN_OFF_CYCLE_TEXT = "Отключить циклическую перезапись";
static const std::string TURN_ON_CYCLE_TEXT = "Включить циклическую перезапись";

class Channel : public Gtk::Widget
{
	public:
		Channel( std::string filename="", std::string tmpdir="", int size=100, int filesize=5000);
		Channel( int size=100, int filesize=5000, std::string filename="", std::string tmpdir="" )
		{
			Channel(filename, tmpdir, size, filesize);
		}
		~Channel();

		// plot
		void draw( double x, double y );
		void replot();
		void clean();

		void set_enabled( bool st );
		inline bool enabled(){ return enable; }
	
		void set_backtrace( bool backtrace );
		inline bool get_backtrace(){ return backtrace; }

		void set_history_max_size( unsigned int pcount );
		void set_history_max_kbytes( unsigned int kbytes );

		inline unsigned int get_history_max_size(){ return history_size; }
		unsigned int get_history_max_kbytes();
		
		void set_size( unsigned int sz );
		void set_color( Gdk::Color clr );
		inline Gdk::Color* get_color(){return &clr;}
		void set_title( const Glib::ustring s );
		inline Glib::ustring get_title()
		{ return curve->title();}

		inline bool empty(){ return data.empty();}
		inline Gtk::CheckButton* get_button() const { return btn; } 
		inline Glib::RefPtr<PlotMM::Curve> get_curve() { return curve; } 
		inline Gtk::Label* get_y_label() const { return lbl_y; } 
		inline Gtk::Frame* get_y_frm() const { return frm_y; } 

		inline static Glib::RefPtr<Channel> create(std::string session, std::string filename="", std::string tmpdir="", int size=100, int filesize=5000)
		{ return Glib::RefPtr<Channel>(new Channel(filename, tmpdir, size, filesize));}

		// return old_fmt;
		std::string set_format_y_label( std::string printf_fmt );
		void show_y_label( bool state );

		// menu
		void menu_color_set_sensitive( bool state );
		void menu_save_set_sensitive( bool state );
		void menu_bufsize_set_sensitive( bool state );
		inline bool menu_color_is_sensitive()
			{ return mi_color->is_sensitive(); }
		inline Gtk::MenuItem* get_menu_color()
			{ return mi_color; }
		
//		inline bool menu_save_is_sensitive()
//			{ return mi_save->is_sensitive(); }
//		inline Gtk::MenuItem* get_menu_save()
//			{ return mi_save; }
		
		inline bool menu_bufsize_is_sensitive()
			{ return mi_bufsize->is_sensitive(); }
		inline Gtk::MenuItem* get_menu_bufsize()
			{ return mi_bufsize; }

		inline Gtk::Menu::MenuList& get_menu()
			{ return m_Menu_Popup.items(); }

		inline Gtk::Menu* get_menu_popup()
			{ return &m_Menu_Popup; }
		
		inline double get_last_x(){ return last_x; }
		inline double get_last_y(){ return last_y; }
		
		inline double get_x_min(){ return x_min; }
		inline double get_x_max(){ return x_max; }
		inline void set_x_min(double x){ x_min = x; }
		inline void set_x_max(double x){ x_max = x; }

		inline std::list<PlotMM::DoublePoint>& get_data(){ return data; }
		inline std::list<History>& get_history(){ return history; }
		inline std::map<double,Time>& get_dataT(){ return historyT; }

		Gtk::Window* mwin;

		inline std::string get_folder(){ return folder; }
		void set_folder(std::string str);
		void set_to_CodePage(std::string to_CodePage);
		void set_from_CodePage(std::string from_CodePage);

		inline std::string get_to_CodePage(){ return to_CodePage; };
		inline std::string get_from_CodePage(){ return from_CodePage; };

		static std::string timeToString(time_t tm=time(0), std::string brk=""); /*!< Преобразование времени в строку HH:MM:SS */
		static std::string dateToString(time_t tm=time(0), std::string brk=""); /*!< Преобразование даты в строку YYYY/MM/DD */
		static bool createDir( const std::string dir );
		
		void timeToInt(long time,int &hour, int &min, int &sec); /*!< Преобразование времени в числа*/

		inline std::string get_historyfiledir(){ return historyfiledir; };
		inline std::string get_historyfilename(){ return historyfilename; };
		inline std::string get_historyfilefullname(){ return historyfilefullname; };
		
		inline FILE* get_outfile(){ return outfile; };
		inline FILE* get_sessionfile(){ return sessionfile; };

		inline long get_file_history_max_point(){ return filesize; };
		void set_file_history_max_point(long value);

		bool readHistory(int num, History& hist);
		bool readNextHistory(History& hist);
		void goHistoryHead();//переход на начало записей истории

		//определяет, находится ли данная точка в пределах области кривой графика.
		bool is_near_the_curve(int x, int y);

		//сохранение .csv
		void save_in_file(std::string filename, double x_m, double x_mx);//to use default x interval x_m and x_mx must be zeroes
		void save_channel_with_save_dialog(std::string name, std::string myname, std::string savefolder);

		//this function removes some blocks of the same code
		void write_to_stringstream(stringstream& s, double x, double y, int hour, int min, int sec);

		//Функции для изменения параметров записи текущей истории на диск
		void set_rewrite(bool state);
		inline bool is_rewriting() { return rewrite; }
		bool startWriting();
		void stopWriting();
		inline bool isWriting() { return is_writing; }

		double into_the_curve(double x);

		inline int get_storage_size() { return stor->get_storage_size(); }//work only when stor is created (stror->create(...) called)
		inline int get_header_size() { return stor->get_header_size(); }
		inline int get_elem_header_size() { return stor->get_elem_header_size(); }
		inline int get_item_size() { return sizeof(History) + get_elem_header_size(); }

		sigc::connection btn_toggled;
		sigc::connection btn_event;

	protected:
		bool on_button_press_event(GdkEventButton *ev);
		void on_event_after( GdkEvent* ev );
		void on_add( Gtk::Widget* w );
		void on_menu_select_color();
		void on_menu_set_k();
		void on_btn_toggled();
		void on_menu_bufsize();

		std::string folder;
		std::string to_CodePage;
		std::string from_CodePage;
		int tm_hour;
		int tm_min;
		int tm_sec;
		int pointnum;//Текущий номер записи в файле
		int filesize;//Максимальное кол-во записей в файле
		
		std::list<History> history;
		std::string historyfiledir;
		std::string historyfilename;
		std::string historyfilefullname;
		
		FILE* outfile;
		int outfileIsOpen;
		FILE* sessionfile;

	private:
		std::list<PlotMM::DoublePoint> data;
		std::map<double,Time> historyT;
		Glib::RefPtr<PlotMM::Curve> curve;
		unsigned int size;//максимальное кол-во точек графика, хранящееся в памяти
		unsigned int cur_history_record;//текущая запись history в памяти
		unsigned int history_size;//максимальное кол-во точек, сохраняемое в памяти, при переполнении графика
		bool enable;
		bool backtrace;
		Gdk::Color clr;
		Gtk::CheckButton* btn;
		Gtk::Label* lbl_y;
		Gtk::Frame* frm_y;
		char vbuf[60];
		std::string y_fmt;	// формат вывода y (на label)
		Gtk::Menu m_Menu_Popup;
		double last_x;
		double last_y;
		double x_min;
		double x_max;

		Gtk::MenuItem* mi_color;
		Gtk::MenuItem* mi_bufsize;

		StorInterface* stor;

		//настройки записи в файл истории
		bool rewrite;//переписывать ли старые данные при переполнении файла хранения истории
		bool is_writing;//идет ли запись данных для канала

		//если конец файла, то пересоздаем его при флаге !rewrite. false, если не удалось
		bool ifEOF();
		//флаг - вести ли записьв файл. Не вести, если сработала проверка ifEOF
		bool write_enabled;

		Time getTime();
};
// -----------------------------------------------------------------------------
#endif // _CHANNEL_H
// -----------------------------------------------------------------------------
