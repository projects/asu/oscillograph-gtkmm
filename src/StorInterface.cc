#include "StorInterface.h"
#include <sys/file.h>

bool StorInterface::open( const std::string name, int max_history_size, int hist_type_size )
{
	#warning открытие файла метод не реализован!
	return false;
}

bool StorInterface::create( const std::string name, int max_history_size, int hist_type_size )
{
	#warning создание файла метод не реализован!
	return false;
}

bool StorInterface::isOpen()
{
	#warning проверка файла метод не реализован!
	return false;
}

bool StorInterface::isFull()
{
	#warning проверка файла на заполненность метод не реализован!
	return false;
}

bool StorInterface::write( History& hist )
{
	#warning запись в файл метод не реализован!
	return false;
}

bool StorInterface::read( int num, History& hist )
{
	#warning чтение из файла метод не реализован!
	return false;
}
